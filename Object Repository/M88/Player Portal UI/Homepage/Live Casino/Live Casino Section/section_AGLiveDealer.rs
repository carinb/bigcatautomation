<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_AGLiveDealer</name>
   <tag></tag>
   <elementGuidId>a70ff951-061b-4421-9f0f-27365f114ac2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='swiper-slide']/div[@id='aglivedealer']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>//div[@class='tiles big-tiles from-desktop-up']/div[@id='club']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
