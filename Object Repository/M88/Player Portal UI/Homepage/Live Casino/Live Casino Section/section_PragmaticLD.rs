<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_PragmaticLD</name>
   <tag></tag>
   <elementGuidId>f09dc524-40bf-467f-be71-8807ed99a1cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='swiper-slide']//div[@id='pragmaticlivedealer']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>//div[@class='tiles big-tiles from-desktop-up']/div[@id='club']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
