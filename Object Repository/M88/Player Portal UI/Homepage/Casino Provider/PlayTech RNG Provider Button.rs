<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PlayTech RNG Provider Button</name>
   <tag></tag>
   <elementGuidId>a6f6895d-8cff-4cb1-ba17-399e4b30cdb1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Provider']//div[contains(@data-component-name,'Playtech')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
