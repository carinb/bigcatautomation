<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>MenuHamburger</name>
   <tag></tag>
   <elementGuidId>aa810c21-3956-41a5-b2e5-546210c44348</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <locator>//label[@class='header-menu-mobile']//img[@class='hamburger-menu']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
