<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Confirm Button</name>
   <tag></tag>
   <elementGuidId>60e7df28-ede4-4baa-a18e-3c3f4337d8d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(@class,'icon-check')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
