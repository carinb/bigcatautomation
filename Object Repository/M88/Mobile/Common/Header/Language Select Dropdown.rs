<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Language Select Dropdown</name>
   <tag></tag>
   <elementGuidId>e3b8473e-e6fc-4686-a005-35e322482076</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav//label[@for='lang-side-menu-toggle']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
