<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Saba Sports Wallet Balance Label</name>
   <tag></tag>
   <elementGuidId>3dd271b6-f1d7-4c09-ad05-76128c99eccc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'msports-icon')]/parent::div//strong[contains(@class,'wallet__total')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
