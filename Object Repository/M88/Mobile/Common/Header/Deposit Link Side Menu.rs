<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Deposit Link Side Menu</name>
   <tag></tag>
   <elementGuidId>67942f70-2680-4c3b-af98-6392a4d8aea8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav//a[contains(@href,'/cashier/deposit')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
