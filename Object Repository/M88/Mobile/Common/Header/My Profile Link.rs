<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>My Profile Link</name>
   <tag></tag>
   <elementGuidId>3419ab21-26ee-427b-a9eb-19873f36574c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav//a[contains(@href,'/account/profile')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
