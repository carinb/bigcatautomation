<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Home Icon</name>
   <tag></tag>
   <elementGuidId>d3c9c606-fa47-4873-8050-9fda52c5f1c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//header//img[contains(@data-src,'logo.png')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
