<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Virtual Games Tab Link</name>
   <tag></tag>
   <elementGuidId>239d009d-006d-4f58-aa59-ab809dddc026</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(@onclick,'/VirtualGames')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
