<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Sport Filter Second Object List</name>
   <tag></tag>
   <elementGuidId>298d65d2-f9fa-49f0-8170-6d6fe1a944c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='ddSport']//div[contains(@class,'dropdownPanel')]//div[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
