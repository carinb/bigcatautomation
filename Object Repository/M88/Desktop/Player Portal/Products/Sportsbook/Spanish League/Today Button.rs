<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Today Button</name>
   <tag></tag>
   <elementGuidId>756902fe-537a-4740-9b9e-ab38ce834c1f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'c-side-sportsmenu')]//span[contains(text(),'Today')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
