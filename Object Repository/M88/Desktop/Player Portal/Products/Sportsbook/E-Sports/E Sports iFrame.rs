<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>E Sports iFrame</name>
   <tag></tag>
   <elementGuidId>ef8c8084-21ff-4857-b5d2-0072aa10ec33</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[contains(@class,'esports-iframe')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
