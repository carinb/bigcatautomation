<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga - Derbi Baccarat Play Now Button</name>
   <tag></tag>
   <elementGuidId>9f7c1cc5-3fc5-4d86-9665-b637d8633184</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='laligaroom']//div[@data-component-name='LaLiga Room - Derbi Baccarat']//div[contains(text(),'Play now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
