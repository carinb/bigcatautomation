<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Laliga Room Slot Section</name>
   <tag></tag>
   <elementGuidId>09361ed4-9d6b-4a9d-8fef-6bbf77b38a7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='tiles laligaroom-tiles']//div[@data-component-name='LaLiga Slots']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
