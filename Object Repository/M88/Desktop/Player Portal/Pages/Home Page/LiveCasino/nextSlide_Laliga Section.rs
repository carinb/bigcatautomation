<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>nextSlide_Laliga Section</name>
   <tag></tag>
   <elementGuidId>33b80348-855d-4d6f-8ff0-54aad7323a93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='laligaroom']//div[contains(@class, 'casino-tiles')]//div[@class='arrow swiper-next' and @aria-label='Next slide']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
