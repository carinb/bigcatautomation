<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_PlayNow Laliga Slot Homepage</name>
   <tag></tag>
   <elementGuidId>36884436-0e43-4840-b2be-a9b940e278a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='laligaroom']//div[@data-component-name='LaLiga Room - Slots']//div[contains(text(),'Play now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
