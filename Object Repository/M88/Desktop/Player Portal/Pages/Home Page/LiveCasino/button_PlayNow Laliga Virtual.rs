<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_PlayNow Laliga Virtual</name>
   <tag></tag>
   <elementGuidId>c8d34c69-a29d-4229-b5b3-caf4d2efd297</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='laligaroom']//div[@data-component-name='LaLiga Virtual']//div[contains(text(),'Play Now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
