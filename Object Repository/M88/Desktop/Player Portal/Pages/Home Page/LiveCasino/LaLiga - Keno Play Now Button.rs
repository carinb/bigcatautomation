<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga - Keno Play Now Button</name>
   <tag></tag>
   <elementGuidId>a4d239e9-53f5-4611-84c6-4092b0217d73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='laligaroom']//div[@data-component-name='LaLiga Room - Keno']//div[contains(text(),'Play now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
