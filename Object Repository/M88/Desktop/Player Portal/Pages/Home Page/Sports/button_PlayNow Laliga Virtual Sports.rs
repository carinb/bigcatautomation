<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_PlayNow Laliga Virtual Sports</name>
   <tag></tag>
   <elementGuidId>203938d2-56bb-4363-9f8d-f6430383705e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sports' and @data-component-name='LaLiga Virtual - EN']//div[contains(text(),'Play Now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
