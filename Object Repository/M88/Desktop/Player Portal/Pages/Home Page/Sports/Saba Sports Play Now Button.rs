<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Saba Sports Play Now Button</name>
   <tag></tag>
   <elementGuidId>60a6b2b7-9da4-4a3b-a099-c936d3405d44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Home Page - Sports']//div[@role='group']//div[@id='sports'][contains(@data-component-name,'SSports')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@data-component-name='Home Page - Sports']//div[@role='group']//div[@id='sports'][contains(@data-component-name,'SSports')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@data-component-name='Home Page - Sports']//div[@role='group']//div[@id='sports'][contains(@data-component-name,'SSports')]</value>
   </webElementXpaths>
</WebElementEntity>
