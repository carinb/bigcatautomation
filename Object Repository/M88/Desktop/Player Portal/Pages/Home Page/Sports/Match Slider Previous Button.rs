<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Match Slider Previous Button</name>
   <tag></tag>
   <elementGuidId>f0efab40-7589-4964-89aa-d292f958ed64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sportsbook']//div[@class='tiles persistent-tiles from-tablet-up']//button[contains(@class,'slick-prev')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@id='sportsbook']//div[@class='tiles persistent-tiles from-tablet-up']//button[contains(@class,'slick-prev')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//button[contains(@class,'slick-prev')]</value>
   </webElementXpaths>
</WebElementEntity>
