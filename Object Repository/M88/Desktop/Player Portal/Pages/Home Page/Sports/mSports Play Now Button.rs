<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>mSports Play Now Button</name>
   <tag></tag>
   <elementGuidId>e5e5f07a-2e62-405d-8b76-37f622222a53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='tile track-events big loaded']//div[@data-url='/msports' and contains(text(),'Play now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@class='tile track-events big loaded']//div[@data-url='/msports' and contains(text(),'Play now')]</value>
   </webElementXpaths>
</WebElementEntity>
