<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>E Sports Play Now Button</name>
   <tag></tag>
   <elementGuidId>65dc1fc2-21fb-4492-b462-43c9c718aca2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Home Page - Sports']//div[@role='group']//div[@id='esports'][contains(@data-component-name,'eSports')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@data-component-name='Home Page - Sports']//div[@role='group']//div[@id='esports'][contains(@data-component-name,'eSports')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//div[@data-component-name='Home Page - Sports']//div[@id='esports'][contains(@data-component-name,'eSports')]</value>
   </webElementXpaths>
</WebElementEntity>
