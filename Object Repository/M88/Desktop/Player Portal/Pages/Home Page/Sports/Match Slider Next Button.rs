<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Match Slider Next Button</name>
   <tag></tag>
   <elementGuidId>062d4b44-1db6-4334-9101-8dac12cf3eb6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sportsbook']//div[@class='tiles persistent-tiles from-tablet-up']//button[contains(@class,'slick-next')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
