<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Sportsbook Section</name>
   <tag></tag>
   <elementGuidId>b7c3b2a6-1cbb-4403-b363-f2e61077beaa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='area new-main-menu left-menu']//a[@id='menu-item-sportsbook']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
