<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Habanero RNG Provider Button</name>
   <tag></tag>
   <elementGuidId>9784b557-2a35-4e69-8cbf-f1afcfcbd555</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Provider']//div[contains(@data-component-name,'Habanero')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
