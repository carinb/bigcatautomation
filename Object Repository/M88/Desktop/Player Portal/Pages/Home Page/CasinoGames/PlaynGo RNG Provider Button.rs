<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PlaynGo RNG Provider Button</name>
   <tag></tag>
   <elementGuidId>8335f2c4-27c7-4764-b2d1-5aed28e3f4e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Provider']//div[contains(@data-component-name,'PlaynGo')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
