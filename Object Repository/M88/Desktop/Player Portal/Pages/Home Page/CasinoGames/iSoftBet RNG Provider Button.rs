<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iSoftBet RNG Provider Button</name>
   <tag></tag>
   <elementGuidId>c1ed8fa1-f3d0-4d2e-a865-474d51d47440</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Provider']//div[contains(@data-component-name,'iSoftbet')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
