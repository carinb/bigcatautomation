<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_PragmaticLiveDealer_PlayNow</name>
   <tag></tag>
   <elementGuidId>cd7539d1-f89a-4843-8f5f-9c14111f9aa0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='live-casino']//div[@id='pragmaticlivedealer']//*[contains(text(),'Play now')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
