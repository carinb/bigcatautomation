<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Hacksaw RNG Provider</name>
   <tag></tag>
   <elementGuidId>fbc9a9c4-e0be-4e0c-9695-eb19dba62ab6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Provider']//div[contains(@data-component-name,'HackSaw - EN')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
