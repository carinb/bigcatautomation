<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Mobile App Section</name>
   <tag></tag>
   <elementGuidId>59e417f2-16c9-4d34-8eb3-7ab2feef9a3d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='block right-menu-item right-menu-download']//a[@href='/mobile' and @id='menu-item-download']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
