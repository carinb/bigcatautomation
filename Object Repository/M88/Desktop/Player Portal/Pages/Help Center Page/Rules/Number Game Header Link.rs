<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Number Game Header Link</name>
   <tag></tag>
   <elementGuidId>e80391c7-1fd7-4015-808a-4e16e2feea4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@href,'rules_numbergame')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
