<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Jackpots Deselect Filter</name>
   <tag></tag>
   <elementGuidId>c8608210-ec88-47aa-9030-1efc5b6608f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'dynamic-content')]//input[@value='Jackpots']/ancestor::div[contains(@class,'round')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
