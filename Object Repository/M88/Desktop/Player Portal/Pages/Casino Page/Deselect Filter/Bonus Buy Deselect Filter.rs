<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Bonus Buy Deselect Filter</name>
   <tag></tag>
   <elementGuidId>42aaff60-001f-4ec6-9304-602a34db0e12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'dynamic-content')]//input[@value='Bonus Buy']/ancestor::div[contains(@class,'round')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
