<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CQ9 Deselect Filter</name>
   <tag></tag>
   <elementGuidId>205e8140-481b-474e-a196-2f6d73687c2c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'dynamic-content')]//input[@value='CQ9']/ancestor::div[contains(@class,'selected-provider')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
