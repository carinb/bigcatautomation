<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PlayNGo Deselect Filter</name>
   <tag></tag>
   <elementGuidId>8117b3a4-d3ae-4601-8bc7-26e11e412881</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'dynamic-content')]//input[@value='PLAYN-GO-GAMES']/ancestor::div[contains(@class,'selected-provider')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
