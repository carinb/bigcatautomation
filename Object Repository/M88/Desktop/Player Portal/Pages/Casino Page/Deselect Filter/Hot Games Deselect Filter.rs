<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Hot Games Deselect Filter</name>
   <tag></tag>
   <elementGuidId>2acceba8-7b8b-4c2a-9164-af91342a1f8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'dynamic-content')]//input[@value='Hot Games']/ancestor::div[contains(@class,'round')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
