<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>First Game Tile Play Now Button</name>
   <tag></tag>
   <elementGuidId>13e62539-9f0c-4fbe-96a3-e8aeededae53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='Casino-Filter-Games']//div[contains(@class,'filter-game')][1]//a[contains(text(),'PLAY NOW')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
