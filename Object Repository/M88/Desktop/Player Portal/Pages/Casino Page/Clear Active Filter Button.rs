<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Clear Active Filter Button</name>
   <tag></tag>
   <elementGuidId>03424d42-5be1-4b26-a8fd-a656a13ad209</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[@class='clear-active-filters']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
