<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga Room - Derbi Baccarat Play Now Button</name>
   <tag></tag>
   <elementGuidId>fe68f0bf-e4fd-4fb9-b6d5-d3139d78ba41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='LaLiga Baccarat']//div[contains(@class,'block hover-area')]//div[contains(text(),'Play now')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
