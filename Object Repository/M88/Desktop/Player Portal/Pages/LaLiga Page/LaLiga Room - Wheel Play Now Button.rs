<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga Room - Wheel Play Now Button</name>
   <tag></tag>
   <elementGuidId>429a628d-0d9c-400a-a6e6-58a889048026</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='LaLiga Wheel']//div[contains(@class,'hover-area')]//div[contains(text(),'Play now')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
