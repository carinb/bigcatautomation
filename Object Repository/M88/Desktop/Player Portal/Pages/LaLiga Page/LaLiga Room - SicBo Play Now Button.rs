<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga Room - SicBo Play Now Button</name>
   <tag></tag>
   <elementGuidId>b88f58e0-454b-4fdf-a834-d01bf74d61b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='LaLiga SicBo']//div[contains(@class,'block hover-area')]//div[contains(text(),'Play now')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
