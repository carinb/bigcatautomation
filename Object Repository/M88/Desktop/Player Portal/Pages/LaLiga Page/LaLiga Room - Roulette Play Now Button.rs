<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LaLiga Room - Roulette Play Now Button</name>
   <tag></tag>
   <elementGuidId>782d1445-de26-4b86-8c86-0ac91c5e43d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-component-name='LaLiga Roulette']//div[contains(@class,'hover-area')]//div[contains(text(),'Play now')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
