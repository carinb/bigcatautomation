<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Slots - LaLiga Champions Play Now Button</name>
   <tag></tag>
   <elementGuidId>5bc7b997-7a26-4e33-956f-7c4ce6efa1e2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-name='Laliga Champion']//div[@class='hover-box']//a[contains(text(),'PLAY NOW')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
