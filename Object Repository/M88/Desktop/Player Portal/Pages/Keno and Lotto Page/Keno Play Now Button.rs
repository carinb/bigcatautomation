<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Keno Play Now Button</name>
   <tag></tag>
   <elementGuidId>7b086233-8456-480e-a4d0-fbbcbbf22918</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='keno-modal-content']//a[contains(@class,'desktop-play-button')][contains(text(),'PLAY NOW')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
