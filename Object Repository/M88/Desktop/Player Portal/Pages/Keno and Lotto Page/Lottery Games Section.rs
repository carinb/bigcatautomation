<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lottery Games Section</name>
   <tag></tag>
   <elementGuidId>6fc6d485-8ac2-4ac0-90ad-e99e77d67554</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='tiles lottery-tiles kenolottopage-tiles big-tiles from-desktop-up']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
