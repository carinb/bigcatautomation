<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lotto Play Now Button</name>
   <tag></tag>
   <elementGuidId>d075599a-44ba-4897-9434-17ff49fe2d39</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='lotto-modal-content']//a[contains(@class,'desktop-play-button')][contains(text(),'PLAY NOW')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
