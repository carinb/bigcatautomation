<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Keno Try Now Button</name>
   <tag></tag>
   <elementGuidId>3ab02820-f839-4693-b795-87023b7c8131</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='keno-modal-content']//a[contains(@class,'desktop-play-button')][contains(text(),'TRY NOW')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
