<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>//div[@data-target='#quick-transfer']</description>
   <name>Quick Transfer Method Button</name>
   <tag></tag>
   <elementGuidId>01052e65-822c-4e8b-93bb-b37537ec0fcf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@data-bank='bt'][contains(@onmouseup,'bt-qt-popup')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
