<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Bank Transfer Button</name>
   <tag></tag>
   <elementGuidId>7a8b6aa2-f95f-43e9-94c8-70de2dacc867</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[contains(@src,'icon-bank-dark')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
