<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>New Bank Name First Record List</name>
   <tag></tag>
   <elementGuidId>5185edfe-f65f-43dd-a919-7a96277a7db7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='select2-newFromBankReference-results']/li[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
