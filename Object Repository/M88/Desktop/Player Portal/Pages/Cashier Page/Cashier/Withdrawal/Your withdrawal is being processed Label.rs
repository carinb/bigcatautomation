<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Your withdrawal is being processed Label</name>
   <tag></tag>
   <elementGuidId>7a17afdd-e6ec-4781-9981-32060ccd9ced</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@class='text'][text()='YOUR WITHDRAWAL IS BEING PROCESSED']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
