<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Virtual Sports Section</name>
   <tag></tag>
   <elementGuidId>6cfbaab6-3ddc-4efa-9515-fd4e11c49fc2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='sports'][contains(@data-component-name,'LaLiga Virtual - EN')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
