<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MYA M88 Logo</name>
   <tag></tag>
   <elementGuidId>0cc3c4bd-516c-499e-ac03-a511e7cb4fc9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[contains(@src,'m88logo')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
