<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Approvals Link</name>
   <tag></tag>
   <elementGuidId>c8a3f9ca-0673-4cf0-b466-38ce80fcc2c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='collapsibleNavItems']//a[text()='Approvals']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
