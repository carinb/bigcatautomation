<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Remove Bank Success Label</name>
   <tag></tag>
   <elementGuidId>7c40279c-9634-443c-a52b-469c62762f23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='Bank account removed']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
