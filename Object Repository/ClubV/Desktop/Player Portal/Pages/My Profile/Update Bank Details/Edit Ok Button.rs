<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Edit Ok Button</name>
   <tag></tag>
   <elementGuidId>8f3e2535-4d00-4c6d-b86b-e4fcb4840f6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Ok')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
