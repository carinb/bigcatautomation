<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Deposit Quick Transfer</name>
   <tag></tag>
   <elementGuidId>ba4e2338-68e2-4f38-9eee-9f3680e6f671</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@class='box bank-card box-small']//img[@src='/images/morph/cashier/logo-deposit-quicktransfer.svg']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
