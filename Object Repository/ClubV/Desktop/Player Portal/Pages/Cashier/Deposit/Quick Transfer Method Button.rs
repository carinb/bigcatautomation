<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Quick Transfer Method Button</name>
   <tag></tag>
   <elementGuidId>24fb3446-fcc1-402b-8068-2515f489d680</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'bank-card')][contains(@onmouseup,'bt-qt-popup')][@data-bank='mp-recom']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
