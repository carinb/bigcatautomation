<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Play Now Button</name>
   <tag></tag>
   <elementGuidId>253058c7-e2bd-4416-bd93-8ddaee52b1cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'filter-game')][1]//div[@class='hover-box']//a[contains(text(),'PLAY NOW')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
