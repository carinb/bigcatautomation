<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pragmatic Radio Button Filter</name>
   <tag></tag>
   <elementGuidId>3b783944-fc55-4c5c-bec8-659b84e281ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popup-modal-filter-body-content']//input[@id='PRAGMATIC']/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
