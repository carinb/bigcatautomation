<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Prestige Level Link</name>
   <tag></tag>
   <elementGuidId>4ce8daff-5a8f-4c03-bee2-83bf77814e36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[text()='PRESTIGE LEVEL']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
