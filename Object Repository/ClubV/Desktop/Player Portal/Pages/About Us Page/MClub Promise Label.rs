<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MClub Promise Label</name>
   <tag></tag>
   <elementGuidId>5dc755eb-c2db-4d62-8529-435e3d79f3d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'desktop-about')]//*[text()='MClub PROMISE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
