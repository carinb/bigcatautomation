<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Powered Label</name>
   <tag></tag>
   <elementGuidId>82e20529-e6e9-43d1-a6f5-2bc1b49cb685</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'desktop-about')]//*[text()='POWERED BY ASIA AND THE WORLD’S LARGEST GAMING SOFTWARE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
