<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>General Label</name>
   <tag></tag>
   <elementGuidId>59a8be4d-e59f-4cc2-be83-0e0b419b25a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='FAQs']/following-sibling::p[text()='General']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
