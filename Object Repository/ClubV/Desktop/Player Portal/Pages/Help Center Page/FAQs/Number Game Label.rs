<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Number Game Label</name>
   <tag></tag>
   <elementGuidId>af1fa5a9-b7fa-4eab-8d69-fc0b990c30c0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='FAQs']/following-sibling::p[contains(text(),'Number Game')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
