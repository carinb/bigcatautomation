<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Payment Methods Label</name>
   <tag></tag>
   <elementGuidId>a6dbecb5-4c01-4b5c-ad23-5fe8a800dfe0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'Payment Methods:')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
