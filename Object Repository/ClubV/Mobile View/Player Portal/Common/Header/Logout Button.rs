<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Logout Button</name>
   <tag></tag>
   <elementGuidId>09447bf1-8b1d-458d-9cbb-9c02e174eeb3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav//a[@href='/account/logout']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
