<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Remove Bank Success Label</name>
   <tag></tag>
   <elementGuidId>85db4d70-61a9-4518-b82a-31e9d5132614</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[text()='Bank account removed']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
