<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Internet Banking Form</name>
   <tag></tag>
   <elementGuidId>e98d89c6-74d8-4fc1-82e0-b9ac4da38d0d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'bt-deposit-channel-popup')][2]//p[text()='Internet Banking']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
