<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Complete Form Button</name>
   <tag></tag>
   <elementGuidId>fd8dc78c-972b-4cbe-9bae-e854e1ddc45b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@ontouchend][contains(@class,'bt-qt-popup')]//a[contains(@onclick,'qt-complete-deposit-popup')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
