<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>My Account Header Link</name>
   <tag></tag>
   <elementGuidId>aac6c7ea-8c85-423f-8955-4920067f7427</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[text()='My Account']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
