<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Responsible Gaming Link</name>
   <tag></tag>
   <elementGuidId>73cc8e49-a6ea-4f98-91cb-03a783b4524d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[text()='Responsible Gaming']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
