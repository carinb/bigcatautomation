<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tutorials Link</name>
   <tag></tag>
   <elementGuidId>74a850bd-97b7-486b-b02d-feffa08fe125</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Tutorials')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
