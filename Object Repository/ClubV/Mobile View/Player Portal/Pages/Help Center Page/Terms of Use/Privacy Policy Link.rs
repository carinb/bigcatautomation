<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Privacy Policy Link</name>
   <tag></tag>
   <elementGuidId>a7332fd2-8ce5-4350-bbbf-aaf64be95e8e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[text()='Privacy Policy']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
