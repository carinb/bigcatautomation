<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Privacy Policy Label</name>
   <tag></tag>
   <elementGuidId>38907327-1468-4af2-9421-cec14b5b6ecb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='Privacy Policy']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
