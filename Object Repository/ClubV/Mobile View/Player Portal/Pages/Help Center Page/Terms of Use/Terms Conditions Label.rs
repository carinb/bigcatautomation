<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Terms Conditions Label</name>
   <tag></tag>
   <elementGuidId>586479b7-1314-4b88-8c5f-d1acd73fb838</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='Terms &amp; Conditions']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
