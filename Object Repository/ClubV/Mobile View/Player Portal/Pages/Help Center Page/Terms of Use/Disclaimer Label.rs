<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Disclaimer Label</name>
   <tag></tag>
   <elementGuidId>de292ec5-0e6b-4e86-b710-21eeece6af24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='Disclaimer']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
