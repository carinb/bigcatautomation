<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Online Deposit Label</name>
   <tag></tag>
   <elementGuidId>d208cb84-b9cc-4cb4-98b9-966a197c04ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[contains(text(),'WEALTHPAY QR')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
