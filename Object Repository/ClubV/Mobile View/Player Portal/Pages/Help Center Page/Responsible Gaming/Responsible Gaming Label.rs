<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Responsible Gaming Label</name>
   <tag></tag>
   <elementGuidId>04e66f20-da85-41b3-aad1-c9200c015b19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[text()='Responsible Gaming']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
