<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Pragmatic Radio Button Filter</name>
   <tag></tag>
   <elementGuidId>b140a859-ff2b-4426-9dc2-6fc4901bd2cb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='popup-modal-filter-body-content']//input[@id='PRAGMATIC']/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
