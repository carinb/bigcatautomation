<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Powered Label</name>
   <tag></tag>
   <elementGuidId>575265d2-00cc-4f3d-988d-8f6864ce4822</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'desktop-about')]//*[text()='POWERED BY ASIA AND THE WORLD’S LARGEST GAMING SOFTWARE']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
