<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Welcome Email Content</name>
   <tag></tag>
   <elementGuidId>1a3faad0-be70-414d-a1c5-fa9159f27b87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//strong[text()='Welcome to MClub!']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
