<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bet_Small</name>
   <tag></tag>
   <elementGuidId>7e5bf3e9-0479-41af-bef8-54f253e828fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[name()='g' and @id='Group']//*[name()='rect' and @id='betspot_small']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value> //div[@class='interaction--4d7ee']//div[@data-role='grid-background']//*[name()='g' and @id='SICBO_DESK_GRID_BACKGROUND']//*[name()='g' and @id='Group']//*[name()='rect' and @id='betspot_small']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <type>Main</type>
      <value>//*[name()='g' and @id='Group']//*[name()='rect' and @id='betspot_small']</value>
   </webElementXpaths>
</WebElementEntity>
