import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLClubV)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Footer/Help Center Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Footer/Help Center Link'))

WebUI.switchToWindowIndex('1')

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Contact Us Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Responsible Gaming Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Responsible Gaming/Responsible Gaming Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/General Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/General Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Deposit Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Deposit Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Withdrawal Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Withdrawal Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/My Account Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/My Account Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Casino Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Casino Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Mobile Header Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Mobile Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Number Game Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/FAQs/Number Game Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Quick Transfer Deposit Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Quick Transfer Deposit Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Online Deposit Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Online Deposit Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Rules/Casino Games Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Rules/Casino Games Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Responsible Gaming Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Responsible Gaming/Responsible Gaming Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Terms Conditions Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Terms Conditions Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Privacy Policy Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Privacy Policy Label'))

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Disclaimer Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Disclaimer Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Header/Contact Us Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Help Center Page/Contact Us/Contact Us Label'))

WebUI.switchToWindowIndex('0')

WebUI.navigateToUrl(GlobalVariable.launchURLClubV)

