import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLClubV)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

CurrentURL = WebUI.getUrl()

WebUI.verifyMatch(CurrentURL, GlobalVariable.launchURLClubV, false)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Featured Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Poker Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Pragmatic Provider Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/PlayNGo Provider Button'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Section'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Hot Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Megaways Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Card Table Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Section'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Pragmatic Provider Button'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/Casino Section'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Home Page/PlayNGo Provider Button'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Home Icon'))

