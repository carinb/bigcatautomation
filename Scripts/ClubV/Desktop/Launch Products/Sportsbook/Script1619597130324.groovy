import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLClubV)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.scrollToPosition(0, 600)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/BTi Sports Tile'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Tile'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/BTi Sports Tile'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iframe'))

WebUI.delay(1)

WebUI.switchToFrame(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iframe'), 0)

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports Content'))

WebUI.switchToDefaultContent()

WebUI.navigateToUrl(GlobalVariable.launchURLClubV)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.scrollToPosition(0, 600)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/BTi Sports Tile'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Tile'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Tile'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/Saba Sports/Saba Sports iFrame'))

WebUI.delay(1)

WebUI.switchToFrame(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/Saba Sports/Saba Sports iFrame'), 0)

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Products/Sportsbook/Saba Sports/Saba Sports Content'))

WebUI.switchToDefaultContent()

