import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Poker Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Poker Link'))

CurrentURL = WebUI.getUrl()

WebUI.verifyMatch(CurrentURL, PokerURL, false)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Play Now Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Hot Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Introduction Section'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Hot Games Label'))

WebUI.delay(1)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Play Now Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Wallet Balance Label'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Transfer Amount Textbox'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Submit Amount Button'))

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Transfer Amount Textbox'), '1000')

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Submit Amount Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Success Play Now Button'))

WebUI.delay(1)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Poker Page/Success Play Now Button'))

WebUI.delay(1)

WebUI.switchToWindowTitle('v8 poker')

WebUI.delay(1)

WebUI.maximizeWindow()

WebUI.delay(10)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Products/Poker/Poker Canvas'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

