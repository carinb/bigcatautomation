import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLClubV)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Login Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Login Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Login Details Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Login Details Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Password Tab Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Password Tab Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Login Email Textbox'))

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Login Email Textbox'), Email)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Password Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Forgot Login Submit Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Login Page/Successful Forgot Login Details Content'))

