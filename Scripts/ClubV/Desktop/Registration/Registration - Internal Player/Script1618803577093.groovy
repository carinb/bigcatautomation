import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLClubV)

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Join Now Link'), 30)

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Join Now Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Join Now Link'))

WebUI.waitForElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Reg Form'), 30)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Name Textbox'), Username)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Username Textbox'), Username)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Password Textbox'), Password)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Email Textbox'), Email)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/MobileNumber Textbox'), MobileNumber)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Submit Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Deposit Link'))

WebUI.verifyTextPresent(Username, false)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Registration Page/Deposit Link'))

WebUI.verifyElementNotPresent(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Dep Form'), 0)

WebUI.mouseOver(findTestObject('ClubV/Desktop/Player Portal/Common/Header/UserAccount Dropdown'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Common/Header/Logout Button'))

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.launchURLMCoreBO)

WebUI.verifyElementVisible(findTestObject('Back Office/Login/Username Textbox'))

WebUI.setText(findTestObject('Back Office/Login/Username Textbox'), GlobalVariable.MCoreBOUser)

WebUI.setText(findTestObject('Back Office/Login/Password Textbox'), GlobalVariable.MCoreBOPass)

WebUI.click(findTestObject('Back Office/Login/Login Button'))

WebUI.waitForElementVisible(findTestObject('Back Office/Common/Players Link'), 0)

WebUI.click(findTestObject('Back Office/Common/Players Link'))

WebUI.waitForElementVisible(findTestObject('Back Office/Players/Main/Username Search Textbox'), 0)

WebUI.setText(findTestObject('Back Office/Players/Main/Username Search Textbox'), Username)

WebUI.click(findTestObject('Back Office/Players/Main/Nickname Search Textbox'))

WebUI.click(findTestObject('Back Office/Players/Main/Search Button'))

WebUI.waitForElementVisible(findTestObject('Back Office/Players/Main/First Record Link'), 0)

WebUI.click(findTestObject('Back Office/Players/Main/First Record Link'))

WebUI.verifyElementText(findTestObject('Back Office/Players/Main/Username Result Label'), Username)

WebUI.verifyElementVisible(findTestObject('Back Office/Players/Main/Internal Player Icon'))

WebUI.closeBrowser()

