import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Casino Link'))

CurrentURL = WebUI.getUrl()

WebUI.verifyMatch(CurrentURL, CasinoURL, false)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/More Filter Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/PlayNGo Radio Button Filter'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/PlayNGo Radio Button Filter'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Apply Filters Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Apply Filters Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Game Card First Index'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Game Card First Index'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Play Now Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/Play Now Button'))

WebUI.delay(3)

WebUI.switchToWindowIndex('1')

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Casino/PNG RNG Canvas'))

WebUI.delay(10)

WebUI.switchToWindowIndex('0')

