import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Home Link'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Home Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Deposit Link Side Menu'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Deposit Link Side Menu'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Withdrawal Link'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Withdrawal Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/From Bank List'))

WebUI.selectOptionByIndex(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/From Bank List'), FromBankIndex)

WebUI.setText(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Amount Textbox'), Amount)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Submit Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Submit Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Confirm Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Confirm Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Withdrawal/Success Label'))

