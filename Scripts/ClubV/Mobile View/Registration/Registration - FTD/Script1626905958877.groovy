import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import javax.lang.model.element.VariableElement as VariableElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.launchURLClubV)

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Join Now Link'), 30)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Join Now Link'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Name Textbox'), Username)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'), Username)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Password Textbox'), Password)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Email Textbox'), Email)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Mobile Number Textbox'), MobileNumber)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Email Textbox'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Complete Registration Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent(Username, false)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Confirm Bonus and Deposit Button'))

WebUI.verifyElementPresent(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Deposit iFrame'), 30)

WebUI.switchToFrame(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Deposit iFrame'), 30)

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Deposit Method Image'))

WebUI.verifyElementPresent(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Bank Transfer Button'), 
    0)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Bank Transfer Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Quick Transfer Method Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Quick Transfer Method Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Complete Form Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Internet Banking Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/To Bank List'))

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Bank Account Name Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/New Bank Name List'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Bank Account Name Textbox'), 
    CurrencyList)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/New Bank Name List'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/New Bank Name First Record List'))

WebUI.delay(1)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Bank Number Textbox'), AccountNumber)

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Save Button'))

WebUI.delay(5)

WebUI.selectOptionByIndex(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/To Bank List'), '1', FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Amount Textbox'), '100')

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Reference ID Textbox'), '123456')

WebUI.verifyElementNotPresent(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/I Dont Want Bonus Label'), 
    0)

WebUI.delay(5)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Next Button'))

WebUI.switchToDefaultContent()

WebUI.delay(10)

WebUI.scrollToPosition(0, 0)

WebUI.switchToFrame(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Deposit iFrame'), 30)

WebUI.waitForElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/From Bank Name Label'), 30)

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/From Bank Name Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Submit Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Cashier/Deposit/Deposit Again Button'))

WebUI.switchToDefaultContent()

WebUI.closeBrowser()

