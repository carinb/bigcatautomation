import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.android.AndroidDriver as AndroidDriver
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.logging.KeywordLogger as KeywordLogger
import com.google.common.collect.ImmutableMap as ImmutableMap
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import io.appium.java_client.AppiumDriver as AppiumDriver
import com.kms.katalon.core.testobject.SelectorMethod as SelectorMethod

Mobile.startExistingApplication('com.android.chrome', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('M88/Mobile/Launch RNG Games/Login/input_Url'), 0)

Mobile.sendKeys(findTestObject('M88/Mobile/Launch RNG Games/Login/input_UrlBar'), 'https://m88.com\\\\n')

not_run: Mobile.tap(findTestObject('M88/Mobile/Launch RNG Games/Login/option1Url'), 0)

WebUI.delay(5)

//driver.getContext()
AppiumDriver<?> driver = MobileDriverFactory.getDriver()

Mobile.comment('0000000000000000000000000000000000000' + driver.getContext())

//Mobile.switchToWebView()
Mobile.tap(findTestObject('M88/Mobile/Launch RNG Games/Login/buttonLogin'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

Mobile.tap(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'), Username)

Mobile.tap(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('ClubV/Mobile View/Player Portal/Pages/Login Page/Password Textbox'), Password)

Mobile.tap(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Login Button'), 0, FailureHandling.STOP_ON_FAILURE)

