import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

CurrentURL = WebUI.getUrl()

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Hot Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Megaways Games Section'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/Casino Page/Card Table Games Section'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Hot Games Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Slot Games Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Card And Table Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Jackpots Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Shooting Games Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 100)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Exclusive Filter Button'))

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Pragmatic Play Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Habanero Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/MicroGaming Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Skywind Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/PlayNGo Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Playtech Tile Filter'))

WebUI.scrollToPosition(0, 200)

WebUI.delay(2)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

