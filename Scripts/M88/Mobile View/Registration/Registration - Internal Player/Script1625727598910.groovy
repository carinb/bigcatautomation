import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import javax.lang.model.element.VariableElement as VariableElement
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.domain)

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Join Now Link'), 30)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Join Now Link'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Name Textbox'), Username)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'), Username)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Password Textbox'), Password)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Mobile Number Textbox'), MobileNumber)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.SelectCurrency'(CurrencyList)

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Email Textbox'), Email)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Email Textbox'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Complete Registration Button'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyTextPresent(Username, false)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Deposit without claiming bonus Link'))

WebUI.delay(2)

WebUI.closeBrowser()

