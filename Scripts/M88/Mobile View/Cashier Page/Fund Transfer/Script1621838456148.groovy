import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Deposit Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Common/Transfer Link'))

WebUI.click(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

GlobalVariable.CentralWalletBefore = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Central Wallet Balance Label'))

GlobalVariable.mSportsWalletBefore = WebUI.getText(findTestObject('M88/Mobile/Common/Header/mSports Wallet Balance Label'))

GlobalVariable.SabaSportsWalletBefore = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Saba Sports Wallet Balance Label'))

WebUI.click(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.FundTransfer'('Central Wallet', 'MSports', '100,00')

WebUI.delay(10)

WebUI.click(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Mobile/Common/Header/Refresh Wallet Button'))

WebUI.delay(3)

GlobalVariable.CentralWalletAfter = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Central Wallet Balance Label'))

GlobalVariable.mSportsWalletAfter = WebUI.getText(findTestObject('M88/Mobile/Common/Header/mSports Wallet Balance Label'))

GlobalVariable.SabaSportsWalletAfter = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Saba Sports Wallet Balance Label'))

WebUI.click(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.ComputeFundTransfer'('Central Wallet', 'MSports', '100,00')

