import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.verifyElementVisible(findTestObject('M88/Mobile/Common/Header/Deposit Link Side Menu'))

WebUI.click(findTestObject('M88/Mobile/Common/Header/Deposit Link Side Menu'))

WebUI.verifyElementPresent(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Deposit iFrame'), 30)

WebUI.switchToFrame(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Deposit iFrame'), 30)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Deposit Method Image'))

WebUI.dragAndDropByOffset(findTestObject('M88/Mobile/Pages/Cashier Page/Deposit/Scroll Mover Section'), 100, 100)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Quick Transfer Method Button'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Quick Transfer Method Button'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Deposit/Complete Form Button'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Mobile/Pages/Cashier Page/Deposit/Internet Banking Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/To Bank List'))

WebUI.delay(5)

WebUI.selectOptionByIndex(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/To Bank List'), ToBankList, 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Amount Textbox'), Amount)

WebUI.setText(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Reference ID Textbox'), ReferenceID)

WebUI.delay(5)

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Next Button'))

WebUI.delay(10)

WebUI.waitForElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/From Bank Name Label'), 
    30)

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/From Bank Name Label'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Submit Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Pages/Cashier/Deposit/Deposit Again Button'))

WebUI.switchToDefaultContent()

