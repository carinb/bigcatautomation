import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.click(findTestObject('ClubV/Mobile View/Player Portal/Common/Header/Hamburger Menu'))

WebUI.verifyElementVisible(findTestObject('M88/Mobile/Common/Header/My Profile Link'))

WebUI.click(findTestObject('M88/Mobile/Common/Header/My Profile Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Update Bank Details Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Update Bank Details Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Add New Account Button'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Add New Account Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Bank Name List'))

WebUI.selectOptionByLabel(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Bank Name List'), 
    BankName, false)

WebUI.setText(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Bank Account Number Textbox'), 
    AccountNumber)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Profile/Update Bank Details/Update Your Account Label'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Save Account Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Profile/Update Bank Details/Add Bank Success Label'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Add Ok Button'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Add Ok Button'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Update Bank Details Link'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Update Bank Details Link'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Remove Account Button'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Remove Account Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Profile/Update Bank Details/Remove Bank Success Label'))

WebUI.verifyElementVisible(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Remove Ok Button'))

WebUI.click(findTestObject('ClubV/Desktop/Player Portal/Pages/My Profile/Update Bank Details/Remove Ok Button'))

