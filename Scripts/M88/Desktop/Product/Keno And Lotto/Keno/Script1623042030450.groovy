import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.mouseOver(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

GlobalVariable.CentralWalletBefore = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Central Wallet Balance Label'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.scrollToPosition(0, 1000)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Results Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Winners Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lottery Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.scrollToPosition(0, 1000)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Try Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Rules Link'))

WebUI.delay(2)

WebUI.switchToWindowTitle('Help Center')

WebUI.switchToWindowIndex('0')

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Play Now Button'))

WebUI.switchToWindowIndex('2')

WebUI.delay(15)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Keno/Keno iFrame'), 0)

not_run: WebUI.verifyTextPresent('Choose your bet limit', false)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Keno/1st Bet button'))

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Keno/Keno iFrame'), 0)

WebUI.delay(2)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Keno/First Range Button'))

WebUI.verifyTextPresent(GlobalVariable.M88Username, false)

WebUI.verifyTextPresent(GlobalVariable.CentralWalletBefore, false)

WebUI.switchToWindowIndex('0')

