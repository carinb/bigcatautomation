import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

not_run: WebUI.mouseOver(findTestObject('M88/Mobile/Common/Header/Show Wallet Balance Button'))

GlobalVariable.CentralWalletBefore = WebUI.getText(findTestObject('M88/Mobile/Common/Header/Central Wallet Balance Label'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.scrollToPosition(0, 1500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Results Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Winners Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lottery Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'), 0)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Rules Link'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

not_run: WebUI.switchToWindowTitle('Help Center')

WebUI.delay(5)

WebUI.verifyTextPresent('Lotto Rules', false)

WebUI.switchToWindowIndex('0')

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Play Now Button'))

WebUI.switchToWindowIndex('2')

WebUI.delay(15)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Keno/Keno iFrame'), 0)

WebUI.verifyTextPresent(GlobalVariable.M88Username, false)

WebUI.verifyTextPresent(GlobalVariable.CentralWalletBefore, false)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Header FAQ Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Header My Bets Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Header My Bets Link'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/My Bets Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Header FAQ Link'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto FAQ My Bets Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto FAQ My Bets Link'), 0)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto FAQ My Bets Link'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/My Bets Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Header FAQ Link'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto FAQ Our Terms Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto FAQ Our Terms Link'))

