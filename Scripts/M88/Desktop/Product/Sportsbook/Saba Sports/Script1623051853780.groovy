import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Section'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'), 0)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Saba Sports iFrame'), 0)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Main iFrame Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result Button'))

WebUI.switchToWindowIndex(1)

WebUI.delay(3)

WebUI.switchToWindowTitle('Result')

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_BetList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_Statement'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_MessageHistoryTab'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_ResultsTab'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Table Game Tab Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sport Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sport Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sport Filter Second Object List'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sport Filter Second Object List'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Result Table'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/League Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/League Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/League Filter Second Object List'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/League Filter Second Object List'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Result Table'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_Statement'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_ResultsTab'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Outright Table'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_BetList'))

WebUI.delay(3)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Keno Lottery iFrame'), 
    0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Keno Lottery Section'))

not_run: WebUI.switchToDefaultContent()

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Table Game Tab Link'))

WebUI.delay(3)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Table Game iFrame'), 
    0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Table Game Section'))

not_run: WebUI.switchToDefaultContent()

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Virtual Games Tab Link'))

WebUI.delay(3)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Virtual Games iFrame'), 
    0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Virtual Games Section'))

not_run: WebUI.switchToDefaultContent()

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/link_MessageHistoryTab'))

WebUI.delay(3)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sports Lottery iFrame'), 
    0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Result/Sports Lottery Section'))

not_run: WebUI.closeWindowTitle('Result')

WebUI.switchToWindowIndex('0')

