import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/B Sports Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/B Sports Section'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iFrame'), 0)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Bet Slip Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Bet Slip Button'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Bet Slip iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Bet Slip Section'))

WebUI.switchToDefaultContent()

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results iFrame'), 0)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Section'))

WebUI.selectOptionByIndex(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Time Range List'), '1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Section'))

WebUI.selectOptionByIndex(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Sport List'), '1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Section'))

WebUI.selectOptionByIndex(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/League List'), '1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Results Section'))

WebUI.switchToDefaultContent()

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Result Close Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Result Close Button'))

WebUI.switchToDefaultContent()

