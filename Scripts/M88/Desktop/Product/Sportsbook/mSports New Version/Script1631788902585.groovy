import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Sports Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Match Slider Next Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Match Slider Previous Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Match Slider Next Button'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Match Slider Previous Button'))

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/mSports Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/mSports Section'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports New Version/mSports Container iFrame'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports New Version/Left Container'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports New Version/Result Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports New Version/Result Button'))

WebUI.switchToDefaultContent()

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result Table'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result League Filter List'))

WebUI.selectOptionByIndex(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result League Filter List'), 
    '0')

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result Table'))

WebUI.selectOptionByIndex(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result Sports Filter List'), 
    '1')

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Result Table'))

WebUI.switchToDefaultContent()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Header Frame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Header Frame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Statement Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Statement Button'))

WebUI.switchToDefaultContent()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Statement Table'))

WebUI.switchToDefaultContent()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Header Frame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Header Frame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Full Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Full Button'))

WebUI.switchToDefaultContent()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'))

WebUI.delay(3)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports Container iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Main Frame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/Full Table'))

