import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Live Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Live Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/Club M88 Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/Club M88 Section'))

WebUI.delay(1)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Live Dealer/M88 Club/M88 Club Canvas'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.closeWindowIndex(0)

