import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Providers Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Providers Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Habanero Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Pragmatic Play Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/MicroGaming Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Skywind Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/CQ9 Deselect Filter'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/PlayNGo Deselect Filter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Playtech Deselect Filter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/iSoftBet Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Habanero Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Habanero Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Habanero Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Pragmatic Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Pragmatic Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Pragmatic Play Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/MicroGaming Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/MicroGaming Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/MicroGaming Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Skywind Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Skywind Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Skywind Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/CQ9 Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/CQ9 Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/CQ9 Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayNGo Games Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayNGo Games Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

not_run: WebUI.scrollToPosition(0, 500)

not_run: WebUI.delay(2)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

not_run: WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

not_run: WebUI.delay(2)

not_run: WebUI.switchToWindowIndex(1)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

not_run: WebUI.delay(1)

not_run: WebUI.closeWindowIndex(1)

not_run: WebUI.switchToWindowIndex(0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/PlayNGo Deselect Filter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayTech Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayTech Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Playtech Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/iSoftBet Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/iSoftBet Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_Spribe Games'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_Spribe Games'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_Spribe'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_BigTimeGaming Games'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_BigTimeGaming Games'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_BigTimeGaming'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_GoldenHero Games'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_GoldenHero Games'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_GoldenHero'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_HacksawGaming Games'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/button_HacksawGaming Games'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_Hacksaw'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

