import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

//not_run: Robot rb = new Robot()
Robot rb = new Robot()

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Area Hot Games'), 0)

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'), 0)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'))

WebUI.delay(4)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'), GameName2)

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox First Record'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox First Record'))

WebUI.delay(20)

WebUI.switchToWindowIndex(1)

WebUI.delay(7)

WebUI.delay(10)

WebUI.getWindowTitle()

WebUI.delay(4)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/buttonGetStarted'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(4)

rb.keyPress(KeyEvent.VK_SPACE)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_SPACE)

WebUI.delay(4)

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.TakeScreenshot'(GameName2)

WebUI.delay(4)

WebUI.closeWindowIndex(1)

WebUI.delay(4)

WebUI.switchToWindowIndex(0)

