import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

Robot rb = new Robot()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox'), GameName)

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox First Record'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Search Textbox First Record'))

WebUI.delay(1)

WebUI.switchToWindowIndex(1)

WebUI.delay(50)

WebUI.getWindowTitle()

WebUI.click(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Casino Game Canvas'), FailureHandling.STOP_ON_FAILURE)

rb.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

rb.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

not_run: CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.TakeRNGScreenshot'(GameName)

not_run: WebUI.clickImage(findTestObject('M88/Desktop/Player Portal/Products/RNG Casino/Button Bet'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.TakeScreenshot'('Sweet M88')

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

