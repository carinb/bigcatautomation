import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Categories Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Categories Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Sort A Z Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Sort A Z Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 400)

