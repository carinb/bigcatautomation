import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Casino Filter Section'), 0)

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.delay(4)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Categories Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Show All Categories Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Hot Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Slot Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Scratch Cards Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Jackpots Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Card And Table Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Video Poker Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Other Games Deselect Filter'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Promotions Games Deselect Filter'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Shooting Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Exclusive Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Hot Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Hot Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Hot Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Slot Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Slot Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Slot Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Scratch Cards Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Scratch Cards Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Scratch Cards Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Jackpots Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Jackpots Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Jackpots Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Card Table Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Card Table Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Card And Table Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Video Poker Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Video Poker Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Video Poker Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Other Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Other Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Other Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Promotion Games Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Promotion Games Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Promotions Games Deselect Filter'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Shooting Games Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Shooting Games Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

not_run: WebUI.delay(2)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Shooting Games Deselect Filter'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Exclusive Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Exclusive Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Exclusive Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Fishing Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Fishing Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Fishing Games Deselect Filter'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Clear Active Filter Button'))

