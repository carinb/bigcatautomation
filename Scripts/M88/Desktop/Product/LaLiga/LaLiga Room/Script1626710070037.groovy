import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Derbi Baccarat Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Slots Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Virtual Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Keno Section'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Roulette Section'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - SicBo Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Slots Section'))

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Wheel Section'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(1)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Virtual Section'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Virtual Play Now Button'))

WebUI.delay(3)

not_run: WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'), 0)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Saba Sports iFrame'), 0)

WebUI.delay(1)

not_run: WebUI.closeWindowIndex(1)

WebUI.delay(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(1)

not_run: WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - SicBo Section'))

WebUI.delay(1)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - SicBo Play Now Button'))

not_run: WebUI.delay(3)

not_run: WebUI.switchToWindowIndex(1)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Canvas'))

not_run: WebUI.delay(1)

not_run: WebUI.closeWindowIndex(1)

not_run: WebUI.delay(1)

not_run: WebUI.switchToWindowIndex(0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(1)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Keno Section'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Keno Play Now Button'))

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Keno Content'))

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.delay(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(1)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Slots Section'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Slots Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Slots Section'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(1)

not_run: WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Roulette Section'))

WebUI.delay(1)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Roulette Play Now Button'))

not_run: WebUI.delay(3)

not_run: WebUI.switchToWindowIndex(1)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Canvas'))

not_run: WebUI.delay(1)

not_run: WebUI.closeWindowIndex(1)

not_run: WebUI.delay(1)

not_run: WebUI.switchToWindowIndex(0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

not_run: WebUI.delay(1)

not_run: WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Wheel Section'))

not_run: WebUI.delay(1)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room - Wheel Play Now Button'))

not_run: WebUI.delay(3)

not_run: WebUI.switchToWindowIndex(1)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Canvas'))

not_run: WebUI.delay(1)

not_run: WebUI.closeWindowIndex(1)

not_run: WebUI.delay(1)

not_run: WebUI.switchToWindowIndex(0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/LaLiga Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Derbi Baccarat Section'))

