import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/1 Million Now Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/1 Million Now Section'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Lotto/Lotto iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/German Keno Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/German Keno Section'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Lotto/Lotto iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Kenow Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Kenow Section'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Lotto/Lotto iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Multi Keno Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Multi Keno Section'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Lotto/Lotto iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Mini Lotto Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Mini Lotto Section'))

WebUI.delay(2)

WebUI.switchToWindowIndex('1')

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Keno and Lotto/Lotto/Lotto iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

