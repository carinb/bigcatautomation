import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Refer Friend link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Refer Friend link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Refer Friend Form'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Refer Friend Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Submit Button'))

WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Refer Friend Textbox'), Email)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Refer Friend Textbox'), 0)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Submit Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Refer Friend Page/Success Label'))

