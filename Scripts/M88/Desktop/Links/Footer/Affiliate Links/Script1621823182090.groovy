import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 3500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Affiliate Link'))

WebUI.delay(10)

WebUI.switchToWindowTitle('M88 Partners')

WebUI.switchToWindowIndex('1')

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Home Icon'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Register Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Home Content'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Commision Header Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Contact Us Header Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Terms and Conditions Header Link'))

WebUI.click(findTestObject('M88/Desktop/Affiliates/Commision Header Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Commision Content'))

WebUI.click(findTestObject('M88/Desktop/Affiliates/Terms and Conditions Header Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Terms and Conditions Content'))

WebUI.click(findTestObject('M88/Desktop/Affiliates/Contact Us Header Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Contact Us Content'))

WebUI.click(findTestObject('M88/Desktop/Affiliates/Home Icon'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Home Content'))

WebUI.click(findTestObject('M88/Desktop/Affiliates/Login Button'))

WebUI.delay(3)

WebUI.switchToWindowTitle(GlobalVariable.MYALoginTitle)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Affiliates/Login Section'))

WebUI.delay(3)

WebUI.closeWindowTitle(GlobalVariable.MYALoginTitle)

WebUI.switchToWindowIndex('0')

