import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 3500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/Help Center Link'))

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Common/Footer/Mobile App Banner'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Payment Methods Label'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Help Center Link'))

WebUI.delay(4)

not_run: WebUI.switchToWindowIndex('1')

WebUI.switchToWindowTitle('M88.com - Help Center: Frequently Asked Questions')

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Contact Us Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Responsible Gaming Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.verifyTextPresent('Responsible Gaming', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/General Header Link'))

WebUI.verifyTextPresent('General', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/Deposit Header Link'))

WebUI.verifyTextPresent('Deposit', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/Withdrawal Header Link'))

WebUI.verifyTextPresent('Withdrawal', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/My Account Header Link'))

WebUI.verifyTextPresent('My Account', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/Casino Header Link'))

WebUI.verifyTextPresent('Casino', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/Mobile Header Link'))

WebUI.verifyTextPresent('Mobile', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/Number Game Link'))

WebUI.verifyTextPresent('Number Game', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/M88 Apps Header Link'))

WebUI.verifyTextPresent('M88 Apps', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/FAQs Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/FAQs/mSports Header Link'))

WebUI.verifyTextPresent('mSports', false)

not_run: WebUI.switchToWindowTitle('1')

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/VND Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Nine Pay Deposit Link'))

WebUI.verifyTextPresent('NINEPAY', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/VND Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Quick Transfer Deposit Link'))

WebUI.verifyTextPresent('Quick Transfer', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/VND Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Online Deposit Link'))

WebUI.verifyTextPresent('EEZIEPAY', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/VND Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Withdrawal Local Link'))

WebUI.scrollToPosition(0, 500)

WebUI.delay(4)

WebUI.verifyTextPresent('LOCAL BANK TRANSFER', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Tutorials Link'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/VND Link'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Tutorials/Withdrawal Online'))

WebUI.verifyTextPresent('Crypto: Wealthpay', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.delay(4)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Sportsbook Header Link'))

WebUI.verifyTextPresent('Betting Rules and Regulations', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Live Casino Header Link'))

WebUI.verifyTextPresent('Casino Club M88', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Casino Games Header Link'))

WebUI.verifyTextPresent('Casino Games', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Keno Header Link'))

WebUI.verifyTextPresent('Keno', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Lotto Header Link'))

WebUI.verifyTextPresent('Lotto & Jackpot Rules', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Number Game Header Link'))

WebUI.verifyTextPresent('Number Game', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Virtual Sports (Sports) Header Link'))

WebUI.verifyTextPresent('Virtual Sports (Sports)', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Rules Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Rules/Bet Builder Header Link'))

WebUI.verifyTextPresent('Bet Builder', false)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Responsible Gaming Link'))

WebUI.verifyTextPresent('Responsible Gaming', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Terms Conditions Link'))

WebUI.verifyTextPresent('Terms & Conditions', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Privacy Policy Link'))

WebUI.verifyTextPresent('Privacy Policy', false)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Terms of Use Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Terms of Use/Disclaimer Link'))

WebUI.verifyTextPresent('Disclaimer', false)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Help Center Page/Header/Contact Us Link'))

WebUI.verifyTextPresent('Contact Us', false)

not_run: WebUI.switchToWindowIndex('0')

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

