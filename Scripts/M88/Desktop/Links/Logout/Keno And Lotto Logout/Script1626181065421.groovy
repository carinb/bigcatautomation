import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLM88)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Vietnam Language List'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Results Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Winners Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lottery Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.scrollToPosition(0, 300)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.switchToWindowIndex(0)

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Keno And Lotto Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Results Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Winners Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lottery Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.scrollToPosition(0, 500)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto View All Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lotto Play Now Button'))

WebUI.delay(2)

WebUI.switchToWindowIndex(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.closeWindowIndex(2)

WebUI.closeWindowIndex(1)

WebUI.closeWindowIndex(0)

