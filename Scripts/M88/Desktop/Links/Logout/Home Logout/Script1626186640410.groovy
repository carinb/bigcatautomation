import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLM88)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Vietnam Language List'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/M88 Logo'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/M88 Logo'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Join M88 CTA Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Join M88 CTA Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Password Textbox'))

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Deposit CTA Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Deposit CTA Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Password Textbox'))

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Play Now CTA Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Play Now CTA Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Registration Page/Password Textbox'))

WebUI.closeWindowIndex(0)

