import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLM88)

WebUI.maximizeWindow()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Common/Header/Language Dropdown'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Vietnam Language List'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Habanero Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Habanero Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Pragmatic Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Pragmatic Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/MicroGaming Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/MicroGaming Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Skywind Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Skywind Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/CQ9 Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/CQ9 Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayNGo Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayNGo Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayTech Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/PlayTech Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Casino Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Modal'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/iSoftBet Games Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/Apply Filters Button'))

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/More Filter/More Filter Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/First Game Tile Play Now Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Username Textbox'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Login Page/Password Textbox'))

WebUI.closeWindowIndex(0)

