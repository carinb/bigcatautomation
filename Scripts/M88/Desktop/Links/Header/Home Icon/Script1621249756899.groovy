import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Header/Home Icon'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Header/Home Icon'))

CurrentURL = WebUI.getUrl()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Sportsbook Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Live Casino Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Mobile App Section'))

WebUI.scrollToPosition(0, 400)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Sportsbook View More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Sportsbook Content'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/B Sports Section'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/eSport Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/mSports Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Saba Sports Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/E Soccer Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Sportsbook Page/Pinnacle Section'), FailureHandling.STOP_ON_FAILURE)

WebUI.back()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Live Casino Section'))

WebUI.scrollToPosition(0, 1500)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Live Casino View More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/AG LD Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/Club M88 Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/LaLiga Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/MicroGaming LD Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/Pragmatic LD Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/Sexy Gaming LD Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Live Casino Page/WM Caisno LD Section'))

WebUI.back()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino Section'))

WebUI.scrollToPosition(0, 300)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Card And Table Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Filter By Provider Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Highly Recommended Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Hot Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Jackpots Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Laliga Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/M88 Exclusive Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/New Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Specials Section'))

WebUI.back()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto Section'))

WebUI.scrollToPosition(0, 400)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Jackpots/Keno Lotto View More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Keno Games Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Results Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Latest Winners Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Keno and Lotto Page/Lottery Games Section'))

WebUI.back()

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Mobile App Section'))

WebUI.scrollToPosition(0, 600)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Mobile Download Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Mobile Download Page/China App Download Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Mobile Download Page/Global App Download Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Mobile Download Page/Download Instructions Section'))

