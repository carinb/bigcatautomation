import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Match Slider Next Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Match Slider Previous Button'))

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/SectionSports'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToPosition(0, 400)

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Match Slider Next Button'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Match Slider Previous Button'))

WebUI.delay(1)

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Sportsbook View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/mSports Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/mSports Play Now Button'))

WebUI.delay(3)

WebUI.switchToWindowIndex('1')

WebUI.delay(3)

WebUI.switchToWindowTitle('Msports - sportsbook')

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'), 0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/mSports/mSports iFrame'))

WebUI.closeWindowIndex('1')

WebUI.switchToWindowIndex('0')

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Saba Sports Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/Saba Sports Play Now Button'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'), 0)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Saba Sports iFrame'), 0)

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Main iFrame Section'))

WebUI.switchToDefaultContent()

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/E Sports Play Now Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/E Sports Play Now Button'))

WebUI.delay(2)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/E-Sports/E Sports iFrame'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/E-Sports/Bet Result Button'))

WebUI.delay(2)

WebUI.switchToDefaultContent()

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/B Sports Play Now Button'))

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/B Sports Play Now Button'))

WebUI.delay(3)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/B Sports iFrame'), 0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/B Sports/Bet Slip Button'))

WebUI.delay(3)

not_run: WebUI.switchToDefaultContent()

not_run: WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 200)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/button_PlayNow Laliga Virtual Sports'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/button_PlayNow Laliga Virtual Sports'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'))

WebUI.delay(2)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'), 0)

WebUI.delay(2)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Saba Sports iFrame'), 0)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Main iFrame Section'))

