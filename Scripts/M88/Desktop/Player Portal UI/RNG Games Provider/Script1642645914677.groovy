import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.launchURLM88)

WebUI.setViewPortSize(390, 844)

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 800)

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/Microgaming RNG Provider Button'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/PlayTech RNG Provider Button'), 
    '87.3594px', '24px')

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/PlayTech RNG Provider Button'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/PlayTech RNG Provider Button'), 
    '12px', '"Open Sans"')

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/Pragmatic RNG Provider Button'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/Pragmatic RNG Provider Button'), 
    'cmyk(26, 35, 50, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/button_BigTimeGaming RNG Provider'), 
    'cmyk(26, 35, 50, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/button_Hacksaw RNG Provider'), 
    'rgba(26, 35, 50, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Casino Provider/Skywind RNG Provider Button'), 
    '87.3594px', '10px')

