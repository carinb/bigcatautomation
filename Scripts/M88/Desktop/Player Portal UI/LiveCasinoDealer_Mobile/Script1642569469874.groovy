import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.setViewPortSize(390, 844)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Live Casino Section'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/section_ClubM88_mobile'))

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/button_ClubM88_PlayNow_mobile'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/section_ClubM88_mobile'), 
    '602.5px', '291.43px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/button_ClubM88_PlayNow_mobile'), 
    '18.7392px', 'AvantGarde-Demi')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/button_ClubM88_PlayNow_mobile'), 
    'rgba(255, 205, 0, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_ClubM88'), 
    '12345_/~/static/images/live-casino/LiveCasino_18/thumbnails/ClubM88Seamless_new_en-US.jpg')

WebUI.clickOffset(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/Live Casino Mobile/section_ClubM88_mobile'), 
    200, 300, FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_AGLiveDealer'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_AGLiveDealer_PlayNow'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_AGLiveDealer'), 
    '602.5px', '12px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_AGLiveDealer_PlayNow'), 
    '100px', 'Open-Sans')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_AGLiveDealer_PlayNow'), 
    'rgba(202, 205, 1, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_AGLiveDealer'), 
    '/~/static/images/live-casino/LiveCasino_18/thumbnails/AG_en-US.jpg')

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_PragmaticLD'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_PragmaticLiveDealer_PlayNow'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_PragmaticLD'), 
    '393.555px', '187.414px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_PragmaticLiveDealer_PlayNow'), 
    '13.12px', 'AvantGarde-Demi')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_PragmaticLiveDealer_PlayNow'), 
    'rgba(255, 205, 0, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_PragmaticLD'), 
    '/~/static/images/live-casino/LiveCasino_18/thumbnails/studioEU_new_en-US.jpg')

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_SexyGaming'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_SexyGaming_Play Now'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_SexyGaming'), 
    '393.555px', '187.414px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_SexyGaming_Play Now'), 
    '13.12px', 'AvantGarde-Demi')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_SexyGaming_Play Now'), 
    'rgba(255, 205, 0, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_SexyGaming'), 
    '/~/static/images/live-casino/LiveCasino_18/thumbnails/AESexy_en-US.jpg')

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_MicrogramingLD'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_MicrogamingLiveDealer_PlayNow'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_MicrogramingLD'), 
    '393.555px', '187.414px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_MicrogamingLiveDealer_PlayNow'), 
    '13.12px', 'AvantGarde-Demi')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_MicrogamingLiveDealer_PlayNow'), 
    'rgba(255, 205, 0, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_MicrogamingLD'), 
    '/~/static/images/live-casino/LiveCasino_18/thumbnails/ClubM88intl_en-US.jpg?v=1    ')

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_MicrogamingLiveDealer_PlayNow'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Live Casino Next Slide'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_LaligaRoom'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_LaligaRoom_PlayNow'))

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyWidthAndHeight'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino Section/section_LaligaRoom'), 
    '393.555px', '187.414px')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyFont'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_LaligaRoom_PlayNow'), 
    '13.12px', 'AvantGarde-Demi')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyBackgroundColor'(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_LaligaRoom_PlayNow'), 
    'rgba(255, 223, 92, 1)')

CustomKeywords.'com.BigcatM88.Utils.PlayerPortalMethods.verifyImgSrc'(findTestObject('M88/Player Portal UI/Homepage/Live Casino/Live Casino SRC Path/src_LaligaRoom'), 
    '/~/static/images/live-casino/LiveCasino_18/thumbnails/LaLiga_en-US.jpg')

