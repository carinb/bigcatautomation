import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Derbi Baccarat Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Virtual'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

not_run: WebUI.verifyElementNotVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - SicBo Section'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/nextSlide_Laliga Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Slot'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Derbi Baccarat Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Slots Section'))

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Derbi Baccarat Section'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Derbi Baccarat Section'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Derbi Baccarat Play Now Button'))

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Canvas'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Virtual'), FailureHandling.STOP_ON_FAILURE)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Virtual'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_PlayNow Laliga Virtual'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3)

not_run: WebUI.switchToWindowIndex(1)

WebUI.delay(1)

not_run: WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Main iFrame Section'), 
    0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'))

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Product iFrame'), 0)

WebUI.switchToFrame(findTestObject('M88/Desktop/Player Portal/Products/Sportsbook/S Sports/Saba Sports iFrame'), 0)

not_run: WebUI.closeWindowIndex(1)

not_run: WebUI.switchToWindowIndex(0)

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

WebUI.delay(1)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Play Now Button'))

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

WebUI.delay(1)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Products/LaLiga/LaLiga Keno Content'))

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Find Out More Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/LaLiga - Keno Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/nextSlide_Laliga Section'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Slot'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Slot'))

WebUI.delay(1)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/LaLiga Room Section'), 0)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/section_Laliga Room Slot Section'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/button_PlayNow Laliga Room Slot'))

WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/LaLiga Page/Slots Section'))

