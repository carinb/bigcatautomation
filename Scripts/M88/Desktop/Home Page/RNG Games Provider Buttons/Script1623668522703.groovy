import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 1000)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Pragmatic RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Pragmatic RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Pragmatic Play Deselect Filter'))

WebUI.back()

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Habanero RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Habanero RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Habanero Deselect Filter'))

WebUI.back()

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Habanero RNG Provider Button'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Microgaming RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Microgaming RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/MicroGaming Deselect Filter'))

WebUI.back()

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Skywind RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/Skywind RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/Skywind Deselect Filter'))

WebUI.back()

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/PlayTech RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/PlayTech RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Playtech Deselect Filter'))

WebUI.back()

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/CQ9 RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/CQ9 RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/CQ9 Deselect Filter'))

WebUI.back()

not_run: WebUI.verifyElementNotVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/PlaynGo RNG Provider Button'), 
    FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/PlaynGo RNG Provider Button'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4)

not_run: WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/PlayNGo Deselect Filter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.back()

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/iSoftBet RNG Provider Button'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/iSoftBet RNG Provider Button'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/iSoftBet Deselect Filter'))

WebUI.back()

WebUI.delay(4)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_BigTimeGaming RNG Provider'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_BigTimeGaming RNG Provider'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_BigTimeGaming'))

WebUI.back()

WebUI.delay(4)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_GoldenHero RNG Provider'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_GoldenHero RNG Provider'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_GoldenHero'))

WebUI.back()

WebUI.delay(4)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_Spribe RNG Provider'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_Spribe RNG Provider'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_Spribe'))

WebUI.back()

WebUI.delay(4)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_Hacksaw RNG Provider'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_Hacksaw RNG Provider'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_Hacksaw'))

WebUI.back()

WebUI.delay(4)

WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/LiveCasino/Casino View More Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_RedTiger RNG Provider'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/CasinoGames/button_RedTiger RNG Provider'))

WebUI.delay(4)

WebUI.scrollToPosition(0, 500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/Casino Page/Deselect Filter/DeselectFilter_RegTiger'))

WebUI.back()

WebUI.delay(4)

