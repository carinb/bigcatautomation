import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.Variable as Variable
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.navigateToUrl(GlobalVariable.launchURLM88)

WebUI.scrollToPosition(0, 3500)

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Common/Footer/About Us Link'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/Twitter Logo'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/YouTube Logo'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/Twitter Logo'))

WebUI.delay(5)

WebUI.switchToWindowIndex(1)

TwitterURLfromPage = WebUI.getUrl()

WebUI.verifyEqual(TwitterURLfromPage, TwitterURL)

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

not_run: WebUI.scrollToPosition(0, 3500)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/YouTube Logo'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/YouTube Logo'))

WebUI.delay(10)

WebUI.switchToWindowIndex(1)

YouTubeURLfromPage = WebUI.getUrl()

WebUI.verifyEqual(YouTubeURLfromPage, YouTubeURL)

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

not_run: WebUI.scrollToElement(findTestObject('M88/Desktop/Player Portal/Pages/Home Page/Sports/mSports Play Now Button'), 0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Common/Footer/About Us Link'))

WebUI.click(findTestObject('M88/Desktop/Player Portal/Common/Footer/About Us Link'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/Opus Gaming Image'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/PlayTech Image'))

WebUI.delay(5)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/Opus Gaming Image'))

WebUI.delay(5)

WebUI.switchToWindowIndex(1)

WebUI.delay(5)

OpusGamingURLfromPage = WebUI.getUrl()

WebUI.verifyEqual(OpusGamingURLfromPage, OpusGamingURL)

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/Opus Gaming Image'))

WebUI.verifyElementVisible(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/PlayTech Image'))

WebUI.delay(3)

WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/About M88 Page/PlayTech Image'))

WebUI.delay(3)

WebUI.switchToWindowIndex(1)

PlayTechURLfromPage = WebUI.getUrl()

WebUI.verifyEqual(PlayTechURLfromPage, PlayTechURL)

WebUI.delay(1)

WebUI.closeWindowIndex(1)

WebUI.switchToWindowIndex(0)

