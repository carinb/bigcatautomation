package com.BigcatM88.Utils

import static com.kms.katalon.core.testobject.ConditionType.EQUALS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.DecimalFormat

import org.apache.commons.lang3.math.NumberUtils

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.support.ui.ExpectedConditions
import com.kms.katalon.core.testobject.SelectorMethod
import org.openqa.selenium.By

import internal.GlobalVariable
public class PlayerPortalMethods {

	@Keyword
	def GetOTP(String Username) {
		String[] unparsedOTP
		String otpCode = ''
		WebUI.navigateToUrl(GlobalVariable.launchURLMCoreBO)
		WebUI.delay(3)
		WebUI.verifyElementVisible(findTestObject('Back Office/Login/Username Textbox'))
		WebUI.setText(findTestObject('Back Office/Login/Username Textbox'), GlobalVariable.MCoreBOUser)
		WebUI.setText(findTestObject('Back Office/Login/Password Textbox'), GlobalVariable.MCoreBOPass)
		WebUI.click(findTestObject('Back Office/Login/Login Button'))
		WebUI.waitForElementVisible(findTestObject('Back Office/Common/Players Link'), 0)
		WebUI.click(findTestObject('Back Office/Common/Players Link'))
		WebUI.waitForElementVisible(findTestObject('Back Office/Players/Main/Username Search Textbox'), 0)
		WebUI.setText(findTestObject('Back Office/Players/Main/Username Search Textbox'), Username)
		WebUI.click(findTestObject('Back Office/Players/Main/Nickname Search Textbox'))
		WebUI.click(findTestObject('Back Office/Players/Main/Search Button'))
		WebUI.waitForElementVisible(findTestObject('Back Office/Players/Main/First Record Link'), 0)
		WebUI.click(findTestObject('Back Office/Players/Main/First Record Link'))
		WebUI.verifyElementText(findTestObject('Back Office/Players/Main/Username Result Label'), Username)
		WebUI.verifyElementVisible(findTestObject('Back Office/Players/Sidebar/Communication Link'))
		WebUI.click(findTestObject('Back Office/Players/Sidebar/Communication Link'))
		WebUI.verifyElementVisible(findTestObject('Back Office/Players/Player Profile/Communication/Search Button'))
		WebUI.click(findTestObject('Back Office/Players/Player Profile/Communication/Search Button'))
		WebUI.verifyElementVisible(findTestObject('Back Office/Players/Player Profile/Communication/Communication First Record Link'))
		WebUI.click(findTestObject('Back Office/Players/Player Profile/Communication/Communication First Record Link'))
		WebUI.switchToFrame(findTestObject('Back Office/Players/Player Profile/Communication/Body iFrame'), 0)
		String textBody = WebUI.getText(findTestObject('Back Office/Players/Player Profile/Communication/Body Section'))

		textBody = textBody.substring(20,50)
		unparsedOTP = textBody.split("");

		for(int a = 0; a < unparsedOTP.length; a++){
			if(otpCode.length()<4){
				if(NumberUtils.isNumber(unparsedOTP[a])){
					otpCode = otpCode + unparsedOTP[a];
				}else{
					otpCode = "";
				}
			}else{
				break;
			}
		}
		GlobalVariable.TransactionOTP = otpCode
		println(GlobalVariable.TransactionOTP)
		WebUI.closeWindowTitle("iCore - Players [" + Username + "]")
	}

	@Keyword
	def SelectCurrency(String currency) {
		String objxpath = '//div[@id="CurrencyDropdown"]//a[@id="'+ currency +'"]'
		TestObject Curr = new TestObject("selectedCurrency")
		Curr.addProperty("xpath", EQUALS, objxpath)
		String obj2xpath

		switch(currency){
			case "CNY": obj2xpath = "//div[@id='PrefixDropdown']//a[@id='+86']";
				break;
			case "THB": obj2xpath = "//div[@id='PrefixDropdown']//a[@id='+66']";
				break;
			case "VND": obj2xpath = "//div[@id='PrefixDropdown']//a[@id='+84']";
				break;
			case "IDR": obj2xpath = "//div[@id='PrefixDropdown']//a[@id='+62']";
				break;
			case "MYR": obj2xpath = "//div[@id='PrefixDropdown']//a[@id='+60']";
				break;
		}

		TestObject Prefix = new TestObject("selectedPrefix")
		Prefix.addProperty("xpath", EQUALS, obj2xpath)

		WebUI.click(findTestObject('Object Repository/M88/Desktop/Player Portal/Pages/Registration Page/Currency List'))
		WebUI.waitForElementVisible(Curr, 30)
		WebUI.click(Curr)

		WebUI.click(findTestObject('Object Repository/M88/Desktop/Player Portal/Pages/Registration Page/Country Code List'))
		WebUI.waitForElementVisible(Prefix, 30)
		WebUI.click(Prefix)
	}

	@Keyword
	def FundTransfer(String sourceWallet, String destinationWallet, String amount){

		switch(sourceWallet) {
			case "SABA Sports": sourceWallet = "2000";
				break;
			case "MSports": sourceWallet = "3000";
				break;
			case "Central Wallet": sourceWallet = "14000";
				break;
		}

		switch(destinationWallet) {
			case "SABA Sports": destinationWallet = "2000";
				break;
			case "MSports": destinationWallet = "3000";
				break;
			case "Central Wallet": destinationWallet = "14000";
				break;
		}

		WebUI.selectOptionByValue(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Transfer/Source Wallet List'), sourceWallet, false)
		WebUI.selectOptionByValue(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Transfer/Destination Wallet List'), destinationWallet, false)
		WebUI.setText(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Transfer/Amount Textbox'), amount)
		WebUI.click(findTestObject('M88/Desktop/Player Portal/Pages/Cashier Page/Cashier/Transfer/Process Transfer Button'))
	}

	@Keyword
	def ComputeValues(String value1, String value2, String operator, String amount){

		String value1a = value1.replace(",","").replace(".","");
		String value2a = value2.replace(",","").replace(".","");
		String amounta = amount.replace(",","").replace(".","");

		DecimalFormat df = new DecimalFormat();
		df.setMinimumFractionDigits(0);
		df.setMaximumFractionDigits(2);

		BigDecimal bdLeft = new BigDecimal(value1a);
		BigDecimal bdRight = new BigDecimal(value2a);
		BigDecimal bdamount = new BigDecimal(amounta);
		BigDecimal bdAnswer = new BigDecimal(0);

		switch(operator) {
			case "+": bdAnswer = bdLeft.add(bdRight);
				break;
			case "-": bdAnswer = bdLeft.subtract(bdRight);
				break;
			case "*": bdAnswer = bdLeft.multiply(bdRight);
				break;
			case "/": bdAnswer = bdLeft.divide(bdRight);
				break;
		}

		if(bdAnswer.equals(bdamount)){
			KeywordUtil.markPassed(value1 + ' ' + operator + ' ' + value2 + ' is equal to ' + amount)
		}else{
			KeywordUtil.markFailed(value1 + ' ' + operator + ' ' + value2 + ' is not equal to ' + amount)
		}
	}

	@Keyword
	def ComputeFundTransfer(String source, String destination, String amount){

		String sourceBefore
		String sourceAfter
		String destinationBefore
		String destinationAfter

		switch(source) {
			case "Central Wallet":
				sourceBefore = GlobalVariable.CentralWalletBefore;
				sourceAfter = GlobalVariable.CentralWalletAfter;
				break;
			case "MSports":
				sourceBefore = GlobalVariable.mSportsWalletBefore;
				sourceAfter = GlobalVariable.mSportsWalletAfter;
				break;
			case "SABA Sports":
				sourceBefore = GlobalVariable.SabaSportsWalletBefore;
				sourceAfter = GlobalVariable.SabaSportsWalletAfter;
				break;
		}

		switch(destination) {
			case "Central Wallet":
				destinationBefore = GlobalVariable.CentralWalletBefore;
				destinationAfter = GlobalVariable.CentralWalletAfter;
				break;
			case "MSports":
				destinationBefore = GlobalVariable.mSportsWalletBefore;
				destinationAfter = GlobalVariable.mSportsWalletAfter;
				break;
			case "SABA Sports":
				destinationBefore = GlobalVariable.SabaSportsWalletBefore;
				destinationAfter = GlobalVariable.SabaSportsWalletAfter;
				break;
		}

		ComputeValues(sourceBefore, sourceAfter, "-", amount)
		ComputeValues(destinationAfter, destinationBefore, "-", amount)
	}

	@Keyword
	def LaunchSEO(String country) {
		String SEOText
		switch(country) {
			case "Thai": SEOText = "M88 Thailand";
				break;
			case "Viet": SEOText = "M88 Vietnam";
				break;
			case "Indo": SEOText = "Bhs Indonesia";
				break;
		}
		String SEOLinkxpath = '//div[@class="localized-links"]//a[text()="' + SEOText + '"]'
		TestObject SEOLink = new TestObject("SEOLink")
		SEOLink.addProperty("xpath", EQUALS, SEOLinkxpath)
		WebUI.waitForElementVisible(SEOLink, 30)
		WebUI.click(SEOLink)
	}

	@Keyword
	def TakeRNGScreenshot(String GameToLaunch) {
		def timestamp = new Date().format("YYYYMMddHHmmss")
		String fileName = GameToLaunch + timestamp
		println(fileName)
		WebUI.takeScreenshot('C:\\Users/BTPQA/Desktop/RNG Screenshot/' + fileName + '.png')
	}

	@Keyword
	def TakeScreenshot(String pFilename) {
		def timestamp = new Date().format("YYYYMMddHHmmss")
		String fileName = pFilename + timestamp
		println(fileName)
		String sysPath = RunConfiguration.getProjectDir()
		WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '.png')
	}


	@Keyword
	def verifyWidthAndHeight(TestObject pObject, String pWidthe, String pHeighte) {
		boolean isPassed
		String thumbnail_width_expected = pWidthe
		String thumbnail_height_expected = pHeighte

		String thumbnailWidthValue = WebUI.getCSSValue(pObject,'width')
		String thumbnailHeightValue = WebUI.getCSSValue(pObject,'height')

		WebUI.comment('[INFO] Expected width: ' + thumbnail_width_expected)
		WebUI.comment('[INFO] Actual width  : ' + thumbnailWidthValue)
		isPassed = WebUI.verifyMatch(thumbnailWidthValue.toString(), thumbnail_width_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] Width matched')
		}else {
			WebUI.comment('[FAILED] Width not matched')
			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyWidth.png')
		}

		WebUI.comment('[INFO] Expected height: ' + thumbnail_height_expected)
		WebUI.comment('[INFO] Actual height  : ' + thumbnailHeightValue)
		isPassed = WebUI.verifyMatch(thumbnailHeightValue.toString(), thumbnail_height_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] Height matched')
		}else {
			WebUI.comment('[FAILED] Height not matched')
			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyHeight.png')
		}
	}


	@Keyword
	def verifyFont(TestObject pObject, String pFontSize, String pFontFamily) {
		boolean isPassed
		String fontSize_expected = pFontSize
		String fontFamily_expected = pFontFamily
		String fontSizeValue = WebUI.getCSSValue(pObject,'font-size')
		String fontFamilyValue = WebUI.getCSSValue(pObject,'font-family')

		WebUI.comment('[INFO] Expected font-size: ' + fontSize_expected)
		WebUI.comment('[INFO] Actual font-size  : ' + fontSizeValue)
		isPassed = WebUI.verifyMatch(fontSizeValue.toString(), fontSize_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] Font-size matched')
		}else {
			WebUI.comment('[FAILED] Font-size not matched')
			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyFontSize.png')
		}

		WebUI.comment('[INFO] Expected font-family: ' + fontFamily_expected)
		WebUI.comment('[INFO] Actual font-family  : ' + fontFamilyValue)
		isPassed = WebUI.verifyMatch(fontFamilyValue.toString(), fontFamily_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] Font-family matched')
		}else {
			WebUI.comment('[FAILED] Font-family not matched')
			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyFontFamily.png')
		}
	}



	@Keyword
	def verifyBackgroundColor(TestObject pObject, String pColor) {
		boolean isPassed
		String objColor_expected = pColor
		String objColorValue = WebUI.getCSSValue(pObject,'background-color')

		WebUI.comment('[INFO] Expected object color: ' + objColor_expected)
		WebUI.comment('[INFO] Actual object color  : ' + objColorValue)
		isPassed = WebUI.verifyMatch(objColorValue.toString(), objColor_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] Object color matched')
		}else {
			WebUI.comment('[FAILED] Object color not matched')
			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyBackgroundColor.png')
		}
	}


	@Keyword
	def verifyImgSrc(TestObject pObject, String pImgSrc) {
		boolean isPassed
		String ImgSrc_expected = pImgSrc
		String ImgSrcValue = WebUI.getAttribute(pObject, 'data-src')

		WebUI.comment('[INFO] Expected img src: ' + ImgSrc_expected)
		WebUI.comment('[INFO] Actual img src  : ' + ImgSrcValue)
		isPassed = WebUI.verifyMatch(ImgSrcValue.toString(), ImgSrc_expected.toString(), false, FailureHandling.CONTINUE_ON_FAILURE)
		if (isPassed) {
			WebUI.comment('[PASSED] File src matched')
		}else {
			WebUI.comment('[FAILED] File src not matched')

			def timestamp = new Date().format("MM-dd-YYYY-HH-mm-ss")
			String fileName = timestamp
			println(fileName)
			String sysPath = RunConfiguration.getProjectDir()
			WebUI.takeScreenshot(sysPath + '/Screenshot/' + fileName + '_FAILED_verifyImgSrc.png')
		}
	}

	@Keyword
	def clickBetButton(TestObject pObject) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 30);

		String strXPATH = pObject.getSelectorCollection().get(SelectorMethod.XPATH)
		try {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(strXPATH))).click();
		}catch(Exception e){
			System.out.println("xpath not found")
		}
	}



	/*	@Keyword
	 def takeScreenshot(String pData){
	 String takeScreenshot = GlobalVariable.TakeScreenshot
	 if(takeScreenshot.toUpperCase() == 'YES'){
	 try{
	 if(GlobalVariable.TestResultPath != ''){
	 int ssCounter = GlobalVariable.ScreenshotCount + 1
	 GlobalVariable.ScreenshotCount = ssCounter
	 String screenshotPath =  GlobalVariable.TestResultPath + '/' + ssCounter + ' - ' + pData
	 WebUI.takeScreenshot(screenshotPath + '.jpg')
	 //			System.out.println('[INFOOOOOOOOOOOOO] Test Suite Level')
	 }else{
	 String sysPath = RunConfiguration.getProjectDir()
	 String ssDate = getDateTime();
	 String testName = GlobalVariable.TestCaseID
	 //			System.out.println('[INFOOOOOOOOOOOOO] GlobalVariable.TestCaseID = ' +GlobalVariable.TestCaseID)
	 //		String testName = ''
	 String folderName = ''
	 //		if(GlobalVariable.TestSuiteID == ''){
	 //			testName = GlobalVariable.TestCaseID
	 //		}else{
	 //			testName = GlobalVariable.TestSuiteID
	 //		}
	 if(testName.contains('/')){
	 String[] strings = testName.trim().split("/");
	 folderName = strings[strings.size() - 1]
	 }else{
	 folderName = testName
	 }
	 //			System.out.println('[INFOOOOOOOOOOOOO] folderName = ' +folderName)
	 String resultPath = sysPath  + "/Reports/" + 'TestCase ' + ssDate + "/" + folderName;
	 File ResultPath = new File(resultPath);
	 if (!ResultPath.exists()) {
	 ResultPath.mkdir();
	 }
	 String screenshotPath = resultPath + '/' + pData
	 WebUI.takeScreenshot(screenshotPath + '.jpg')
	 //			System.out.println('[INFOOOOOOOOOOOOO] Test Case Level')
	 }
	 }catch(Exception e){
	 WebUI.comment('[ERROR] Unable to take screenshot due to: ' +e)
	 }
	 }
	 }*/
}
