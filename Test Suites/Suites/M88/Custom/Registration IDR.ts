<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Registration IDR</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>cc948fc6-ed42-4beb-9539-f711befddca8</testSuiteGuid>
   <testCaseLink>
      <guid>1c9c185c-feeb-42d6-a0b4-119be32e59cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Custom/Registration - MYA Banner</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7850309b-e2fe-4250-a39b-2d72604c06aa</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/STG IDR</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>7850309b-e2fe-4250-a39b-2d72604c06aa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>a2fcce20-4563-47a7-9ba9-c3c11a97ec8f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7850309b-e2fe-4250-a39b-2d72604c06aa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>8a089792-2143-4b85-bbdd-3d44931747c9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7850309b-e2fe-4250-a39b-2d72604c06aa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>MobileNumber</value>
         <variableId>dd877673-05c7-4792-9247-3af74df50a89</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7850309b-e2fe-4250-a39b-2d72604c06aa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>f6c93fd8-5e3e-48b9-9ba7-36087d53fc7d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7850309b-e2fe-4250-a39b-2d72604c06aa</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>CurrencyList</value>
         <variableId>b9487d5c-6606-4c26-ae2a-c4fbaacf71ea</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
