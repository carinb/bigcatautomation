<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MYA M88 Logo - Sheet 6</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>81cf53de-3881-46dd-bfb6-79bec3d86e65</testSuiteGuid>
   <testCaseLink>
      <guid>05c40add-64c0-4d82-9ff4-71e6dcca36b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Custom/Registration - MYA M88 Logo</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>63afd0f6-4698-40de-9ec4-ccebddce0a90</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Custom/MYA Sheet 6</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>63afd0f6-4698-40de-9ec4-ccebddce0a90</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>2a1bdf6f-2ae4-45e0-9ee2-bf5566cf02db</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>63afd0f6-4698-40de-9ec4-ccebddce0a90</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>e4885786-73c9-4a3f-a790-933e56183aeb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>63afd0f6-4698-40de-9ec4-ccebddce0a90</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>MobileNumber</value>
         <variableId>d58d4061-020e-4658-8cfc-a5c48e14b179</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>63afd0f6-4698-40de-9ec4-ccebddce0a90</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>98f3c8e5-3b36-4ee8-97f6-5c475dcb80ba</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>63afd0f6-4698-40de-9ec4-ccebddce0a90</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>CurrencyList</value>
         <variableId>e8bd314c-6329-4cc9-8d10-63f0ee20f56b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
