<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Keno And Lotto</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>b150b541-cfd0-43c4-88c5-740302a4d26a</testSuiteGuid>
   <testCaseLink>
      <guid>50ceb40c-eff7-496f-8de1-be1799f75a1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>21f3154a-8f63-4973-b6ee-02e21df7c73a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>317124e9-1c51-4bac-aaf7-f81e92471fc6</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>317124e9-1c51-4bac-aaf7-f81e92471fc6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>317124e9-1c51-4bac-aaf7-f81e92471fc6</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>92c31a9e-5006-4928-8377-531bbd7c7595</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Keno And Lotto/Keno</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>462ff704-7d50-496e-9fc9-2cb198e6f362</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Keno And Lotto/Lotto</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
