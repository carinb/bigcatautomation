<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sportsbook</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d9d67f49-6788-4ccf-be96-f5882235d39d</testSuiteGuid>
   <testCaseLink>
      <guid>3e93a3e8-069d-47aa-8e66-cae7dca74f77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5e5df2ea-1903-4125-a00d-66d1ba47de40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>45ba686a-de02-426f-bc6f-09dc0842abf7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>45ba686a-de02-426f-bc6f-09dc0842abf7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>45ba686a-de02-426f-bc6f-09dc0842abf7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0192df6f-7c2c-4801-8340-b133861a3d60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/Saba Sports</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b819a27c-b5f8-4fed-ac7b-b5bb868681cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/mSports New Version</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f05114b5-9ea2-468a-a7ef-e127558bf2c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/mSports</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>137ebd6e-33b9-4439-9234-cd1eff031304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/E Sports</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b2b7bf3-8dd1-4417-adf6-fd482499e176</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/M88 Spanish League</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d23f8356-b510-46e9-a4b8-4e456f99b6cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/Sportsbook/B Sports</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
