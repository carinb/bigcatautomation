<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Registration in BO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>21e81dd4-de6c-404c-918a-01664639b2fe</testSuiteGuid>
   <testCaseLink>
      <guid>57509a35-f59d-4cf9-beeb-f3dae2afe9ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/BO Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b051ab6e-71e8-49a9-90dc-5462780fb557</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Check Real Player Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d7c9c173-908a-4a5d-a0a2-16dcc1d020b9</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Registration/Smoke Test Prod</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d7c9c173-908a-4a5d-a0a2-16dcc1d020b9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>f3671400-1a96-407f-8ddb-786f139e1057</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e07fe46c-8ff7-4bbb-8bbc-942683afd70f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Check Paid SEO Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>77dde10a-932a-4293-82a8-bf43c296bffd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Smoke Test Prod</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>77dde10a-932a-4293-82a8-bf43c296bffd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>bbeb0edd-29b5-45ff-9db0-a70ae4d6c5cc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>975bfae6-81c6-4436-bc1f-7960ffb275b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Check Affiliates Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d04b831a-5f9d-4a35-8f35-c9ee1a8a96f0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Registration - My Affiliates D</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>d04b831a-5f9d-4a35-8f35-c9ee1a8a96f0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>bbeb0edd-29b5-45ff-9db0-a70ae4d6c5cc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51ef2c9e-b458-418d-aec3-7aa5084da9cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Check RAF Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>30cd75a2-a6c5-4e4e-bde0-e6dad7f0af56</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Registration - RAF D</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>30cd75a2-a6c5-4e4e-bde0-e6dad7f0af56</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>bbeb0edd-29b5-45ff-9db0-a70ae4d6c5cc</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
