<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Header Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7de17bec-390a-43af-8c74-26c403c4a551</testSuiteGuid>
   <testCaseLink>
      <guid>48616397-0a02-41d9-bb93-c708a78dfdca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>77673210-ec99-4e72-a98e-6502a36c160d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>77673210-ec99-4e72-a98e-6502a36c160d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>77673210-ec99-4e72-a98e-6502a36c160d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f6705010-ec91-482d-89e6-1767a2c074f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Announcement Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f98fd085-e9f9-4572-816f-f2fe4e638793</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Casino Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>087a8172-9bea-45a9-bd17-7547f6e2452d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0a965443-eff7-4c83-9fd9-ef3c76a55a13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Home Icon</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>645898e4-2813-4092-9304-3f5fd5d37572</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>517801a6-e884-41bb-a6da-2bc64b8f106b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Live Casino Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92d94b98-079b-4050-bea9-16063ba5517f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Poker Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>06056595-468b-4706-a237-933d52720aae</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>12210d65-499d-452d-939c-2458a69eef23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Promotions Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>813eba98-0c5a-49ed-ae6c-5162aa4f766b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f2a2c2de-e05e-42e0-acf9-5f05c3530b3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Sports Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>336f354d-5f6c-43d7-aea3-bd169fefcbd4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b273e3dc-05a1-4fec-88bb-a92c7344f16f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a5b33866-dd41-45aa-b5cc-1e37bcd3a3c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/VIP Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6e761fa-e931-47b4-abbf-bc257b2b3da7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/Refer Friend Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b66d7b-faad-449a-b482-378213cb536a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Header/App Link</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
