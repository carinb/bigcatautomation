<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Footer Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>aa65b639-e85a-4aff-986b-ffce94424d73</testSuiteGuid>
   <testCaseLink>
      <guid>5613723a-055a-4a98-8ad0-86bf968b2ded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0e65e686-0b9d-4c1a-82c2-507409ac70d7</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>0e65e686-0b9d-4c1a-82c2-507409ac70d7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>0e65e686-0b9d-4c1a-82c2-507409ac70d7</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b88d1c75-3c69-4813-bc01-0160a0805be7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/About Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ffb4b90-23b1-4719-81ff-783b2aee03cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Bangking Options</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cc12668-fee7-4d75-ac89-3a19eef6120c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Contact Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bab0db40-2697-48e9-a2f9-c54953ff23f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Refer Friend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecf2c33e-1cc5-452d-ba22-bf04360d1a24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Responsible Gaming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0c9d16c-ac83-4b75-a5a8-999c4d55b8e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/VIP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8a83eeb-d114-479e-b2fa-f82c7df88767</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Affiliate Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a24a7b86-7db8-4c30-b821-7fed7fa27d59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Links/Footer/Help Center Links</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
