<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LaLiga</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5e073669-9062-4b01-8a47-49e34402a7a1</testSuiteGuid>
   <testCaseLink>
      <guid>217d6247-0f27-4a31-96c5-c6c65385df1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8d4cd97e-eb60-4332-ac07-84cbe09e7b73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>769d1da4-2850-4440-9d28-2a368a0663b4</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>769d1da4-2850-4440-9d28-2a368a0663b4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>769d1da4-2850-4440-9d28-2a368a0663b4</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0782f89c-6eca-4431-8806-e7d3787907ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/LaLiga/LaLiga Room</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb3a72fe-0e5a-4e89-9a90-ddc14bf0d4ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/LaLiga/Derbi Baccarat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8766fc2d-3a53-43d3-817c-d84abe59a59c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Product/LaLiga/LaLiga Slot</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
