<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Emails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5c4dada8-7565-4f14-beb9-b40208991b9e</testSuiteGuid>
   <testCaseLink>
      <guid>048d1cc8-a420-4e5d-80e7-fa27acc5c814</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Password Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3494e671-23a1-475b-8dc2-7297d7f7ea0e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Portal Journey D</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3494e671-23a1-475b-8dc2-7297d7f7ea0e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>d7c40d73-b933-4b1b-b738-91dc0f99961c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3e4a0c30-b64b-4b58-a436-a30e85425200</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Username Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9c45b24c-e9b1-41d6-92e8-f1379dd82e36</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Portal Journey D</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>9c45b24c-e9b1-41d6-92e8-f1379dd82e36</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>cd5a7a21-ba28-4e0b-800c-4108611fe61b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
