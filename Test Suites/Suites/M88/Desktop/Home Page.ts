<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Home Page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c9c542f8-1c78-4f4f-81cd-fe46ea6db5a8</testSuiteGuid>
   <testCaseLink>
      <guid>f109afb6-8517-42e1-a6e4-ec7d453ea8c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f372a7b0-8783-412b-b6b6-392084a30242</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Player Journey</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f372a7b0-8783-412b-b6b6-392084a30242</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>4b12b931-7a3b-4b06-b7b6-53bac91921c1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f372a7b0-8783-412b-b6b6-392084a30242</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>d52ca407-6293-41b9-8ce2-bc30e15195f0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dbd205a2-a306-411d-bb2d-73c905165dcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6bf4255a-55ee-49d9-aaa8-af301e162c60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/RNG Games Provider Buttons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85fcd920-2a48-4502-8bc5-2ca5eebaf48d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/Sportsbook Play Now Buttons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8ba735c-4a32-4c69-814d-62180a42d1c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/Keno Lotto Play Now Buttons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>919c8890-5d86-4a46-887b-3b2c239e8756</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/Partners Logo Buttons</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e7e864b4-02c2-4ff4-90cb-bbd58c19736f</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Partner Urls</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5c9a5436-f3f1-41ee-a8cf-c7e7e757c325</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e7e864b4-02c2-4ff4-90cb-bbd58c19736f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>TwitterURL</value>
         <variableId>879fa61c-c151-4ed6-ab66-5d3b874328a0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e7e864b4-02c2-4ff4-90cb-bbd58c19736f</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>YoutubeURL</value>
         <variableId>fe1dd0ac-228c-4c6f-9049-0d785c836052</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>40dd7448-8dda-4815-8875-11f89659c756</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a3f62b2a-6995-41ae-8d86-1f6f9cb09066</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cdc3635c-783b-413e-8c68-7d03c240bee0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>86d4dc0f-a533-4ff6-b193-216154899f28</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1b0f5f02-ed23-4e12-b871-ee64a6264f6d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>87116dda-6af1-453e-b444-32aa8951caa2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7a6e648e-9a6b-4bbd-8744-05c81f39e452</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ba5a6b98-5e7a-4f31-bb66-ca15568e8e1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/LaLiga Play Now Buttons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>082f0ac9-77a3-4d1a-ba9d-2f86af0cd99d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Home Page/Live Casino Play Now Buttons</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6a0d1faf-86c4-4aa4-ba0d-38235d4c3dcf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>65fdaabd-48e5-428d-9df8-c4666888c4d0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a7a7b8e1-28c7-4cb4-b695-84ad9d33a894</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2114ec87-4ebc-4da1-859a-e0c7bb4b0e4c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
