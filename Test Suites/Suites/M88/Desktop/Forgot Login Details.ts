<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Forgot Login Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>217ed507-68ea-4427-b59f-ff6be4ede0ee</testSuiteGuid>
   <testCaseLink>
      <guid>078fa0b9-4bb6-4955-8c68-961832c5418d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0f42b24c-9cf7-41c6-9ca9-c8133b12340a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Forgot Login Details/Forgot Password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>77ebd556-08a5-4672-81ce-3d338d42e69a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Forgot Login Details</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>77ebd556-08a5-4672-81ce-3d338d42e69a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>3d4a7798-52c8-430c-8979-3324228cc055</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>50d4488e-599a-4552-a1dc-1c1080331407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Forgot Login Details/Forgot Username</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b2ff6b59-0149-4808-a26c-30e6a15420dd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Forgot Login Details</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b2ff6b59-0149-4808-a26c-30e6a15420dd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>ddb04f1e-b155-4327-982e-4d4452a72f4e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
