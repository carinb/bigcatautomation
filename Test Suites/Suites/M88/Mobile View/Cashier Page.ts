<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cashier Page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>623278b5-d4bd-4eaf-829a-f45c04d36c22</testSuiteGuid>
   <testCaseLink>
      <guid>976fb0e9-68f7-4021-b55a-0fe95b86079d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6a27cd87-02ff-451e-9080-80b349fbaddf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Domains</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>6a27cd87-02ff-451e-9080-80b349fbaddf</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>domain</value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7a77f2c9-c81d-41b5-ae74-c33344874eb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8b37b6b1-641f-4b04-9262-f6e0d3931561</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Player Portal Journey M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8b37b6b1-641f-4b04-9262-f6e0d3931561</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>7f488562-1881-4b77-b13a-1d639c1f707e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8b37b6b1-641f-4b04-9262-f6e0d3931561</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>028ba0a7-27e0-4027-97ec-80391e4919f9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3eecc67f-0e28-4302-b87a-7cf5c26e6355</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>597312ac-2132-41d6-97e3-629164ab7da4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/Update Language</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a380c4d6-28a2-4bde-bde1-dec3a296f566</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/View Inbox</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df6a2b35-0e1f-4409-a242-5d502266f726</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/View Deposit Method</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f54e2b3-ace6-4703-918c-837a518eab2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/Submit Deposit Request</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2ec956df-a8bd-4e3f-8e47-c58ca2fec204</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9261d381-9eb0-49e1-bd8e-ef1f9292e8af</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46fe2a08-2622-42b3-a7f2-5a432afe341e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>25070ece-372a-426d-a790-cf028061dcd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/Submit Withdrawal Request</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c8a9ca13-4ec0-4f39-a7e9-d2ea833d18a1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>26bed66d-8f86-44db-87d7-6a370b3ffc03</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f4362c65-8388-4399-9ce4-8aaa73f480d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/View Deposit Withdrawal History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f57af41-918f-4147-bd92-8377d2955d38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/View Betting History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8122e551-4225-4f25-aea0-ce1e952c8f40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/View Fund Transfer History</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
