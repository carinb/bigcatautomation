<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Footer Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>224acbc1-7447-4570-987c-4298f1d79aff</testSuiteGuid>
   <testCaseLink>
      <guid>fd953822-d7f5-4136-9e85-bea2d11cd0c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>daa8ee55-e43b-4b2c-85e2-29ab66c93d2e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Domains</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>daa8ee55-e43b-4b2c-85e2-29ab66c93d2e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>domain</value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9f42ddf4-6397-46ff-a807-d3526a7914e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c627135b-6f8d-4804-a14c-3162127e3318</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Player Portal Journey M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c627135b-6f8d-4804-a14c-3162127e3318</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>7f488562-1881-4b77-b13a-1d639c1f707e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c627135b-6f8d-4804-a14c-3162127e3318</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>028ba0a7-27e0-4027-97ec-80391e4919f9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3eecc67f-0e28-4302-b87a-7cf5c26e6355</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9d6bc445-e88d-412a-9a95-9aeff600637a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/About Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39c8cadb-d451-450d-87a4-9f42efc35752</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/VIP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01e7457e-e3fe-428e-8af4-80e200608903</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Bangking Options</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ef4081c-95a8-43b8-9cfe-b9dff1548cc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Contact Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2b1826b-8bd4-406a-978f-c05bec9352e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Help Center Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a57e4836-2c13-427e-9b58-98dd03838ec2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Refer Friend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bdcb938-a96f-48f4-870c-823472a9ad3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Responsible Gaming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3bd7f31-3f88-4c41-abd8-386f6dea5cdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Links/Footer/Affiliate Links</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
