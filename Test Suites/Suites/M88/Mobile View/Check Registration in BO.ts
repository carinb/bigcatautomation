<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Registration in BO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>68a01766-0fa8-496e-815a-163b1d1e41b1</testSuiteGuid>
   <testCaseLink>
      <guid>57509a35-f59d-4cf9-beeb-f3dae2afe9ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Common/BO Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b051ab6e-71e8-49a9-90dc-5462780fb557</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Common/Check Real Player Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b58e045f-5525-4beb-947e-ee23a0940227</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Registration - Real Player M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b58e045f-5525-4beb-947e-ee23a0940227</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>911b4880-99f6-4b46-a266-5b5826e1c3e0</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
