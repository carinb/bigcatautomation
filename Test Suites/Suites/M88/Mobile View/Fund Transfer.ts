<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Fund Transfer</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ac96260a-1900-4492-b3bd-652e9124c38b</testSuiteGuid>
   <testCaseLink>
      <guid>0bb6fadc-5a41-443a-ac28-43926bd17194</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Desktop/Common/Get Domain URL</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>19456089-750b-4d28-80aa-75ef1b84ec54</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Domains</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>19456089-750b-4d28-80aa-75ef1b84ec54</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>domain</value>
         <variableId>8e509231-c8a1-467b-8735-785fec9af9d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7a27fa27-1f30-4d6a-9188-02201536ce12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>fd65d26e-0598-4a59-8a09-35e606d000bd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Player Portal Journey M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>fd65d26e-0598-4a59-8a09-35e606d000bd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>7f488562-1881-4b77-b13a-1d639c1f707e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>fd65d26e-0598-4a59-8a09-35e606d000bd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>028ba0a7-27e0-4027-97ec-80391e4919f9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3eecc67f-0e28-4302-b87a-7cf5c26e6355</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1b33f4ea-4116-445c-b836-18c786e7cbb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/M88/Mobile View/Cashier Page/Fund Transfer</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
