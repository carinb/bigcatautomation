<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Emails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e439d93b-4bf1-41fc-ae95-3bfb33ea7260</testSuiteGuid>
   <testCaseLink>
      <guid>597ab2c8-6083-4fbd-83f3-7595bfd44919</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Password Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>435cacc3-2aae-490c-9ddf-1e14d0a71ade</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Player Portal Journey M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>435cacc3-2aae-490c-9ddf-1e14d0a71ade</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>d7c40d73-b933-4b1b-b738-91dc0f99961c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3e42ace7-437e-4f95-9f0d-dde23d9617ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Username Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>40f15720-1667-4135-b1c8-9f4f1ec5cf11</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Mobile View/Player Portal Journey M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>40f15720-1667-4135-b1c8-9f4f1ec5cf11</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>cd5a7a21-ba28-4e0b-800c-4108611fe61b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
