<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Emails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>90</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>978aea9a-2877-4bb5-be3f-add8d9a45e56</testSuiteGuid>
   <testCaseLink>
      <guid>bd178070-c2fe-4135-b729-32093ec1861d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Common/Check Welcome Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b18a6cbc-e092-490e-9a16-b10236afb4e2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Registration - Real Player M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b18a6cbc-e092-490e-9a16-b10236afb4e2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>76c033b4-a2bb-4e7a-b27b-42f97dc637c4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aa231391-ccf9-41db-88c7-6054fc35b00d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Common/Check Forgot Password Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>49ae9cb4-2b8a-42ae-bc4c-4efacf42bf0d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>49ae9cb4-2b8a-42ae-bc4c-4efacf42bf0d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>cd6fb0c0-2202-4d06-bedd-85948017469d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e68f5d6d-cbe6-4f07-8feb-f0fe0c00d86f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Common/Check Forgot Username Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>382a2dea-84f0-44a9-9a2d-79387ca00365</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>382a2dea-84f0-44a9-9a2d-79387ca00365</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>fe1a4a8d-bf5e-4743-a494-2abb0a01045b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
