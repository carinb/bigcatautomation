<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Footer Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5cd74904-b9fe-45d8-b94d-365d418d5409</testSuiteGuid>
   <testCaseLink>
      <guid>668fba62-9243-49bc-b2dc-174db59fb7df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>925e4da8-c6db-4406-9720-dfdc427b5ae8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>925e4da8-c6db-4406-9720-dfdc427b5ae8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>5537cc99-5cfb-4d73-8a31-2d355f324ef6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>925e4da8-c6db-4406-9720-dfdc427b5ae8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>807aa2e7-9fa1-4f79-a60b-5a5ff8080bde</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bbb38482-3b71-46af-8594-65139163cad1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/Contact Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a325d3ff-3f37-4552-89bd-f6eebbae3ba7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/Help Center Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6082820-a2dc-4bab-8330-ed39016a1dca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/About Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39cd0fa0-52da-414c-b337-c8a6ee86a003</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/Refer Friend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0902d72-ebe2-43d3-a56a-965e8c20e92c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/VIP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c33c03a8-1a4e-4607-ad75-7aee285970a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/Bangking Options</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bb67eb0-064c-4894-951e-6d6989975c9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Links/Footer/Responsible Gaming</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
