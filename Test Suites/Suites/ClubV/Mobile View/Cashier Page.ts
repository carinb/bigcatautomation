<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cashier Page</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a64b156a-1136-4dad-a60e-8dce0d2d633c</testSuiteGuid>
   <testCaseLink>
      <guid>76ec26cb-5600-4088-9398-b75a0c37fd02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>4d51fbac-eb07-4c3d-a5ac-076c533637cf</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>4d51fbac-eb07-4c3d-a5ac-076c533637cf</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>5537cc99-5cfb-4d73-8a31-2d355f324ef6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>4d51fbac-eb07-4c3d-a5ac-076c533637cf</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>807aa2e7-9fa1-4f79-a60b-5a5ff8080bde</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51704b74-c813-432f-a022-1dfbed32fbe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/Update Language</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>357f2661-5a4d-4b3a-b093-89d2acd52b78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/View Inbox</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea443079-f595-4b7b-98bf-7057a03636b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/View Deposit Method</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26872b4f-7016-4d21-9865-97f78645c4f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/Submit Deposit Request</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4823782e-8cfe-4831-8d52-52695de07c13</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a0a95f5d-b5f5-433a-b3ca-82d16a9e1157</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24f79132-b2e0-460c-9166-a6bfc1f19716</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b81a8ae3-1370-4011-86e0-f175620111a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/Submit Withdrawal Request</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed78386b-ccc1-4b23-9d90-129d824f0b4a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5e5b3ce0-0d06-4a37-931f-7922de3d96c8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>40d6a57f-c38b-4c55-952e-8ddaf54419d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/View Deposit Withdrawal History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9960d9c-d75c-4ea9-98a2-acdf3cd61897</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Cashier Page/View Betting History</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
