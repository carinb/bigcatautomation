<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Forgot Login Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1983f65c-c2fd-4ebf-8aee-54c9a7e24623</testSuiteGuid>
   <testCaseLink>
      <guid>bc73f821-eea1-4870-ab56-c8483c11dfc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Forgot Login Details/Forgot Password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c67d97f9-d14d-421a-a20e-5168e79bc633</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c67d97f9-d14d-421a-a20e-5168e79bc633</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>346eaea0-58ee-4315-9e71-5e3570449136</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6317c70b-b236-46c2-917e-0dd2073b77b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Mobile View/Forgot Login Details/Forgot Username</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c79328a2-4859-437d-a8eb-f02546a64767</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Player Portal Journey - Mobile</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c79328a2-4859-437d-a8eb-f02546a64767</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>eedcc74c-9c9f-4a35-a5b1-631c0518dadb</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
