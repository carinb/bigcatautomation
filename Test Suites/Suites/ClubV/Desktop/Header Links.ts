<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Header Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>572db940-d80b-4a42-9dc6-ade78ebe6fa7</testSuiteGuid>
   <testCaseLink>
      <guid>bcc02072-31b0-4de5-abc9-cf4b10322c52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>10e5880c-e28e-45fb-a395-41374d3303cc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>10e5880c-e28e-45fb-a395-41374d3303cc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>63cd2a99-fabe-42a1-8072-e30c572c0383</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>10e5880c-e28e-45fb-a395-41374d3303cc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>38cb66e4-42bf-492e-8ebb-46fd51508d9c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>accd2495-fa2b-4c30-be30-51cad04e85e3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c5f711-31c4-4620-b86e-de8a82a78316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Home Icon</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4488bed5-d4f4-49b4-8b6f-df5e90002a8e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a9df6ef7-ed64-4bbd-a1f3-14134f1d9a18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Sports Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>394a3f6a-7700-43ca-970d-d14f3c4dd3c2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f75d2aea-a5d9-41da-bd9b-e2507db07eb7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6f742a96-c38a-47c9-89ef-60b93dba8c65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Live Casino Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>375d7bcb-1b0f-42dc-8b54-3283638370b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Casino Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2d969b33-9a2e-47b9-b2da-9a3039f1b361</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>773a452b-b491-47bf-ad95-4511dd7b865f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>95482282-4d4f-4560-a1f2-649044b3cad5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Poker Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e2a179b1-bfa7-4052-a157-3a9a6593bf7b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>59e1f99d-a6a0-4c3d-940e-333c9d85f8b0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>89ecae56-9540-46b0-b656-40e72bdc425a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Promotions Link</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4d833cdd-f8d1-4a07-a705-abcdc3c338fd</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52e9d293-e9cb-4c38-aba9-fadd2356ff0a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3adf2087-19e0-4994-9aa0-a4bc886e1ab0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/VIP Links</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
