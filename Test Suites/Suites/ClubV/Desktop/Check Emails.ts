<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Emails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f99e43c8-943c-4138-ac1d-467339c8f8e9</testSuiteGuid>
   <testCaseLink>
      <guid>048d1cc8-a420-4e5d-80e7-fa27acc5c814</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Password Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>50dc498e-1021-4069-993c-3b18d1e30992</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>50dc498e-1021-4069-993c-3b18d1e30992</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>d7c40d73-b933-4b1b-b738-91dc0f99961c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3e4a0c30-b64b-4b58-a436-a30e85425200</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Forgot Username Email</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>37634dc6-da20-4241-9dcf-e9bd9c6a0ed0</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>37634dc6-da20-4241-9dcf-e9bd9c6a0ed0</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>cd5a7a21-ba28-4e0b-800c-4108611fe61b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
