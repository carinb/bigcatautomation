<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Footer Links</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>30239227-48d8-4d17-8c79-4a0a2defee16</testSuiteGuid>
   <testCaseLink>
      <guid>84f29b05-c6ea-4080-9f1a-9262f7c65a97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Player Portal Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f25f3a30-dadd-459f-8ba6-e986665c88d3</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>f25f3a30-dadd-459f-8ba6-e986665c88d3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>63cd2a99-fabe-42a1-8072-e30c572c0383</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>f25f3a30-dadd-459f-8ba6-e986665c88d3</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>38cb66e4-42bf-492e-8ebb-46fd51508d9c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>accd2495-fa2b-4c30-be30-51cad04e85e3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>73510d19-6d83-475b-b672-e773aeab79b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/Help Center Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>645203c8-8503-4e90-90a7-413dff6a49a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Header/Announcement Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c0f30eb-b370-4587-9838-8f9e9a43220d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/About Us</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9e842c2-80cb-49d6-96a0-44314aa35ee9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/Refer Friend</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e72bd324-e389-4334-a8e4-87c81ae35215</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/VIP</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>759552ca-c0ce-4289-a373-9791f99d4a03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/Bangking Options</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab6f6f3d-3440-41da-b2cd-bf50747f61b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/Responsible Gaming</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae516db3-6a79-408d-ad7c-dab454def366</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Links/Footer/Contact Us</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
