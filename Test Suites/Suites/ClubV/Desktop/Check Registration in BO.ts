<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Check Registration in BO</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f6639622-0410-47b0-891a-a00f6f9bfe72</testSuiteGuid>
   <testCaseLink>
      <guid>57509a35-f59d-4cf9-beeb-f3dae2afe9ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/BO Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6656354-3e88-406b-a7a9-e09d1ce16016</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Internal Player Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>098eb335-bf22-42c8-808d-97dc81498424</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Registration - Internal Player M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>098eb335-bf22-42c8-808d-97dc81498424</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>3a30b8e9-6d94-4418-a92e-1d8cbd8762df</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b051ab6e-71e8-49a9-90dc-5462780fb557</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Common/Check Real Player Registration in BO</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>074cc4b9-9b0f-4a74-9656-ddb460c4ad4b</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Mobile/Registration - Real Player M</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>074cc4b9-9b0f-4a74-9656-ddb460c4ad4b</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>3ad03535-1c86-4a70-8bed-b4121d63ffed</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
