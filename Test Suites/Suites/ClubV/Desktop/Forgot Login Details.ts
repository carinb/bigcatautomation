<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Forgot Login Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>60</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>cb01083f-88b3-4c86-a150-101b57c4d1ba</testSuiteGuid>
   <testCaseLink>
      <guid>3c29f6d9-5eeb-45df-b7ce-ebc1443b7d1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Forgot Login Details/Forgot Password</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3211659e-1a61-4ba3-bd74-faf208f2ca9a</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3211659e-1a61-4ba3-bd74-faf208f2ca9a</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>d304b903-bf84-4676-a070-8035e43a5f99</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6ac0ef2e-fa8b-4050-96ed-9a78e67acf69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/ClubV/Desktop/Forgot Login Details/Forgot Username</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3eca7334-518e-4ffb-9d0b-38734b3b6782</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/ClubV/Desktop/Player Portal Journey - Desktop</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3eca7334-518e-4ffb-9d0b-38734b3b6782</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>91ce9890-8ae2-40a0-a595-bbb232365886</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
