<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Evolution M88</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>dd80c86f-63b6-49d3-bc41-7f427ff88370</testSuiteGuid>
   <testCaseLink>
      <guid>337817aa-d4e4-4523-be99-ca955826a9bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Player Portal Login_M88</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c7b76042-e413-4a1f-b004-c5b487ff97b5</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/M88/Custom/Evolution Player Journey_M88</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>c7b76042-e413-4a1f-b004-c5b487ff97b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Username</value>
         <variableId>b5939c79-81d8-4487-be45-f361d3ac27ac</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c7b76042-e413-4a1f-b004-c5b487ff97b5</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>261cd545-2f4e-401b-86bf-5b7e011a4b89</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bbf2a558-9280-4ea6-9b17-7142d98c456a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>837a9f2c-2a60-44e5-a5f4-e159505b4640</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Baccarat A</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>46cd3784-adae-47f2-accd-22785067168b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bb0dd289-12bd-4b99-b6b9-218b7ee930a7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1d097fa4-4a3f-4d4d-9190-986ead58a16d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Baccarat B</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ebeed9bf-7259-43e4-add3-dbad879834fc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e00fac4a-e067-4c5c-a577-4478e3ff6ae7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>16631d29-0efe-4af2-95d9-6c1ee181e6c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Baccarat C</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>764e9d9d-0c08-4bc3-b75f-7e60157e7275</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e5dfe3db-59c0-42c2-9703-df2dd5d8498c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5ee5f4ec-ccee-4a18-9224-b8f80444c0c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Speed Baccarat A</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9769d34b-5eb3-4dd4-a615-e294d084ef79</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e7706209-e1cd-4047-ac66-3602e9ba9567</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3fcb5cba-0356-4885-97f4-86c89b0b490d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Speed Baccarat B</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4ac9060c-99ed-4568-8261-32be9fa0904c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d753ed3a-ae4d-46e7-bb80-e879f2b97348</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fc994c11-448d-46b3-b5ea-a68c8209bd0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Speed Baccarat C</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>17720370-5154-4740-98f0-ecfc2111f460</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>da715602-dc7c-4e14-b5f9-e4854ee95916</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c59458b4-15be-402e-9a15-293f2a959e1c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Speed Baccarat D</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ccdef9e4-cadd-461f-9ea2-f2b1b8edbba8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>703c9e2b-bd4d-473d-ad11-1896403fdd3c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>956e0f15-ef8b-43ca-be7b-218eb5b1c61c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Speed Baccarat E</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dad40892-c3a0-45c0-9799-2573310fc2cc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0d1efa0f-443a-41db-ac3f-4770782046e2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ea66cb5a-afb6-4269-853b-b03fae5bb1bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Baccarat Squeeze</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>872aa2fb-a010-4ca8-ae24-7ea4b8446f0e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c540563c-662b-47e8-b816-b53e8eb3f250</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>05b4c5ba-c3e5-4d1f-86fc-723b94c11f77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Baccarat/Evolution Baccarat Control Squeeze</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cbdcfa4b-0ca5-41ef-87eb-fa10ef7ebf63</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>35fd64c9-f11e-4a7b-b251-72331def9840</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3b609ef0-2a5f-4186-87e7-f18e853e6e29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Dragon Tiger/Evolution DragonTiger</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>efe7f534-a6ba-4a9a-83ca-982286408f79</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bf5e2348-7af7-4ddc-8947-e5ae1a0d1a3e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ef3d4d5b-d0b4-4ffa-9e7a-50d6a1cf6fb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution GameShow/Evolution CrazyTime</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>40d5f0b1-469d-47d8-af1a-a7f07735c536</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ef6b3b91-191c-47a3-8349-57dffeebb3ee</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c1bd072c-c7f3-4f74-bb51-edd6b0cb6f66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution GameShow/Evolution DreamCatcher</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fab490f4-8ae2-4f72-ae0b-8e06671fb4ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5c4b0ba8-d9ab-43e9-a67f-80b1ef1d0242</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6d132698-aead-45ad-b05b-91ea9281589a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Custom/Evolution_M88/Evolution Sic Bo/Evolution SicBo</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f1aa5a4-53e3-4e11-baff-3e03173e7fb1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5dd01a47-84b7-41d3-ac55-eefa62a12b52</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
